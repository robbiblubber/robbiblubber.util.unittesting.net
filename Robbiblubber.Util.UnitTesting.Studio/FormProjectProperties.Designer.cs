﻿namespace Robbiblubber.Util.UnitTesting.Studio
{
    partial class FormProjectProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProjectProperties));
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._CheckParallel = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(305, 120);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(125, 28);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Tag = "utstdo::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(174, 120);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Tag = "utstdo::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _TextName
            // 
            this._TextName.Location = new System.Drawing.Point(23, 39);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(407, 25);
            this._TextName.TabIndex = 0;
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(20, 23);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(77, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "utstdo::udiag.projp.name";
            this._LabelName.Text = "Project &Name:";
            // 
            // _CheckParallel
            // 
            this._CheckParallel.AutoSize = true;
            this._CheckParallel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckParallel.Location = new System.Drawing.Point(137, 75);
            this._CheckParallel.Name = "_CheckParallel";
            this._CheckParallel.Size = new System.Drawing.Size(299, 21);
            this._CheckParallel.TabIndex = 3;
            this._CheckParallel.Tag = "utstdo::udiag.projp.parallel";
            this._CheckParallel.Text = "   Enable &parallel test execution for this project";
            this._CheckParallel.UseVisualStyleBackColor = true;
            // 
            // FormProjectProperties
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(453, 178);
            this.Controls.Add(this._CheckParallel);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormProjectProperties";
            this.Tag = "utstdo::udiag.projp.caption";
            this.Text = "Properties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.CheckBox _CheckParallel;
    }
}