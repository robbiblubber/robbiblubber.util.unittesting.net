﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>Main form.</summary>
    public partial class FormMain: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Test host.</summary>
        private HTMLOutputTestHost _Host = new HTMLOutputTestHost();
        
        /// <summary>Currently dragged node.</summary>
        private TreeNode _DragNode = null;
        
        /// <summary>Drop target.</summary>
        private TreeNode _DropTarget = null;
        
        /// <summary>Allow drop operation.</summary>
        private bool _AllowDrop = false;
        
        /// <summary>Show debug menu.</summary>
        private bool _ShowDebug = false;
        
        /// <summary>Top node of running test.</summary>
        private TreeNode _RunningNode = null;
        
        /// <summary>Running flag.</summary>
        private bool _Running = false;
        
        /// <summary>Aborting flag.</summary>
        private bool _Aborting = false;

        /// <summary>Recent file list.</summary>
        private RecentFileList _RecentFiles;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="file">File name.</param>
        public FormMain(string file = null)
        {
            InitializeComponent();

            PathOp.Initialize("robbiblubber.org/utstdo");

            Locale.UsingPrefixes("debugx", "utstdo");
            Locale.LoadSelection("Robbiblubber.Util.NET");
            this.Localize();

            __LoadSettings();

            _RecentFiles = new RecentFileList(PathOp.UserConfigurationPath + @"\recent.files", 6);
            _UpdateRecentFiles();

            _PanelBrowser.Dock = DockStyle.Fill;
            _Host.Updated += _Host_Updated;
            _Host.Clear();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal properties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the debug menu is shown.</summary>
        internal bool __ShowDebug
        {
            get { return _ShowDebug; }
            set { _ShowDebug = _MenuDebug.Visible = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves settings.</summary>
        internal void __SaveSettings()
        {
            try
            {
                Ddp cfg = new Ddp();

                cfg.SetValue("/settings/level", (int) UnitTest.Level);
                cfg.SetValue("/settings/debug", __ShowDebug);

                cfg.Save(PathOp.UserSettingsFile);
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2010", ex); }
        }


        /// <summary>Lodas settings.</summary>
        internal void __LoadSettings()
        {
            try
            {
                if(!File.Exists(PathOp.UserSettingsFile))
                {
                    __SaveSettings();
                    return;
                }

                Ddp cfg = Ddp.Load(PathOp.UserSettingsFile);

                UnitTest.Level = ((ReportingLevel) cfg.GetInteger("/settings/level"));
                __ShowDebug = cfg.GetBoolean("/settings/debug");
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2011", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Draws the current project.</summary>
        private void _DrawProject()
        {
            try
            {
                _TreeProject.BeginUpdate();

                _TreeProject.Nodes.Clear();

                TreeNode m = new TreeNode(TProject.Current.Name);
                m.Tag = TProject.Current;
                m.ImageKey = "project";

                foreach(IUnitTest i in TProject.Current.Items)
                {
                    m.Nodes.Add(_NodeFromTest(i));
                }

                _TreeProject.Nodes.Add(m);
                m.Expand();

                _SplitMain.Visible = true;

                _TreeProject.EndUpdate();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2012", ex); }
        }


        /// <summary>Creates a tree node for a library.</summary>
        /// <param name="lib">Libarary.</param>
        /// <returns>Tree node.</returns>
        private TreeNode _NodeFromLibrary(TLibrary lib)
        {
            try
            {
                TreeNode m = new TreeNode(lib.Name);
                m.Tag = lib;

                m.ImageKey = m.SelectedImageKey = (lib.FileName.ToLower().EndsWith("exe") ? "program" : "dll");

                foreach(IUnitTest i in lib)
                {
                    m.Nodes.Add(_NodeFromTest(i, true));
                }

                return m;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2013", ex); }

            return null;
        }


        /// <summary>Creates a tree node for a test.</summary>
        /// <param name="test">Test.</param>
        /// <param name="inlib">Determines if the test is in a library.</param>
        /// <returns>Tree node.</returns>
        private TreeNode _NodeFromTest(IUnitTest test, bool inlib = false)
        {
            try
            {
                if(test is TLibrary) { return _NodeFromLibrary((TLibrary) test); }

                TreeNode m = new TreeNode(test.Name);
                m.Tag = test;
                m.ImageKey = m.SelectedImageKey = (inlib ? "ltest" : "test");

                return m;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2014", ex); }

            return null;
        }


        /// <summary>Inserts a node.</summary>
        /// <param name="node">Node.</param>
        private void _InsertNode(TreeNode node)
        {
            try
            {
                if((_TreeProject.SelectedNode == null) || (_TreeProject.SelectedNode.Tag is TProject))
                {
                    _TreeProject.Nodes[0].Nodes.Insert(0, node);
                }
                else
                {
                    TreeNode before = _TreeProject.SelectedNode;
                    if(!(before.Parent.Tag is TProject)) { before = before.Parent; }

                    before.Parent.Nodes.Insert(before.Index + 1, node);
                }
                node.EnsureVisible();
                _TreeProject.SelectedNode = node;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2015", ex); }
        }



        /// <summary>Updates the sort indexes.</summary>
        private void _Resort()
        {
            try
            {
                int m = 0;
                foreach(TreeNode i in _TreeProject.Nodes[0].Nodes)
                {
                    ((IUnitTest) i.Tag).SortIndex = m++;

                    int n = 0;
                    foreach(TreeNode j in i.Nodes) { ((IUnitTest) j.Tag).SortIndex = n++; }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2016", ex); }
        }
        

        /// <summary>Checks if the project needs to be saved befor closing.</summary>
        /// <returns>Returns TRUE if project can be closed, otherwise returns FALSE.</returns>
        private bool _CommitClose()
        {
            try
            {
                if(TProject.Current == null) { return true; }
                if(TProject.Current.Virgin) { return true; }

                string mtext = "utstdo::udiag.savechanges.text".Localize("The project contains unsaved changes. Do you want to save these changes before proceeding?");
                string mcaption = "utstdo::udiag.savechanges.caption".Localize("Save Changes");
                switch(MessageBox.Show(mtext, mcaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        return _Save();
                    case DialogResult.No:
                        return true;
                    case DialogResult.Cancel:
                        return false;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2017", ex); }

            return false;
        }


        /// <summary>Saves the current project.</summary>
        /// <returns>Returns TRUE if the project has been saved, otherwise returns FALSE.</returns>
        private bool _Save()
        {
            try
            {
                if(string.IsNullOrWhiteSpace(TProject.Current.FileName)) { return _SaveAs(); }

                TProject.Current.Save();
                _RecentFiles.Add(TProject.Current.FileName);
                _UpdateRecentFiles();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2018", ex); }

            return true;
        }



        /// <summary>Saves the current project.</summary>
        /// <returns>Returns TRUE if the project has been saved, otherwise returns FALSE.</returns>
        private bool _SaveAs()
        {
            try
            {
                SaveFileDialog f = new SaveFileDialog();
                f.Filter = "utstdo::udiag.filter.uttxp".Localize("Test Execution Plans") + " (*.uttxp)|*.uttxp|" + "utstdo::udiag.filter.all".Localize("All Files") + "|*.*";

                if(f.ShowDialog() == DialogResult.OK)
                {
                    TProject.Current.Save(f.FileName);

                    _RecentFiles.Add(f.FileName);
                    _UpdateRecentFiles();

                    return true;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2019", ex); }

            return false;
        }


        /// <summary>Runs the test for a given node.</summary>
        /// <param name="node">Tree node.</param>
        private void _Run(TreeNode node)
        {
            try
            {
                _RunningNode = node;
                _PanelRun.Visible = true;

                _Prepare(node);

                _Running = true;
                _TimerRun.Enabled = true;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2020", ex); }
        }


        /// <summary>Prepares node for execution.</summary>
        /// <param name="node">Tree node.</param>
        /// <param name="recursive">Recursive flag.</param>
        private void _Prepare(TreeNode node, bool recursive = false)
        {
            try
            {
                if(node.Tag is TProject)
                {
                    node.ImageKey = node.SelectedImageKey = "project_wait";
                }
                else if(node.Tag is TLibrary)
                {
                    node.ImageKey = node.SelectedImageKey = ((TLibrary) node.Tag).LibraryTypeString + "_wait";
                }
                else
                {
                    if(node.Parent.Tag is TLibrary)
                    {
                        node.ImageKey = node.SelectedImageKey = "ltest_wait";
                    }
                    else { node.ImageKey = node.SelectedImageKey = "test_wait"; }

                    ((IUnitTest) node.Tag).Data = new TestExecution((IUnitTest) node.Tag);

                    if(node.Parent.Tag is TProject)
                    {
                        if(node.Index == 0) { ((ITestHostWriter) ((TestExecution) ((IUnitTest) node.Tag).Data).Host).WriteHeader(((TProject) node.Parent.Tag).Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); }
                    }
                    else
                    {
                        if(node.Index == 0)
                        {
                            if(node.Parent.Index == 0)
                            {
                                ((ITestHostWriter) ((TestExecution) ((IUnitTest) node.Tag).Data).Host).WriteHeader(((TProject) node.Parent.Parent.Tag).Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            }

                            ((ITestHostWriter) ((TestExecution) ((IUnitTest) node.Tag).Data).Host).WriteHeader(((TLibrary) node.Parent.Tag).Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                    }

                    ((ITestHostWriter) ((TestExecution) ((IUnitTest) node.Tag).Data).Host).WriteTestHeader((IUnitTest) node.Tag);
                    return;
                }

                foreach(TreeNode i in node.Nodes) { _Prepare(i, true); }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2021", ex); }
        }


        /// <summary>Runs available tests.</summary>
        /// <param name="node">Tree node.</param>
        /// <param name="running">Counts currently running tests.</param>
        /// <param name="syncWait">Determines if a waiting test requires serial execution.</param>
        private void _RunStep(TreeNode node, ref int running, ref bool syncWait)
        {
            try
            {
                if((node.Tag is TProject) || (node.Tag is TLibrary))
                {
                    foreach(TreeNode i in node.Nodes)
                    {
                        _RunStep(i, ref running, ref syncWait);
                    }
                    return;
                }
                if((running > 0) && syncWait) return;

                TestExecution ex = ((TestExecution) ((IUnitTest) node.Tag).Data);

                switch(ex.State)
                {
                    case TestExecution.ExecutionState.READY:
                        running++;
                        syncWait = (!(ex.Test.Parallelizable && TProject.Current.AllowParallel));
                        node.ImageKey = node.SelectedImageKey = ((node.Parent.Tag is TLibrary) ? "ltest_run" : "test_run");
                        ex.Execute();
                        break;
                    case TestExecution.ExecutionState.EXECUTING:
                        running++;
                        if(!node.ImageKey.EndsWith("_run")) { node.ImageKey = node.SelectedImageKey = ((node.Parent.Tag is TLibrary) ? "ltest_run" : "test_run"); }
                        break;
                    case TestExecution.ExecutionState.FAILED:
                        if(running == 0) { ex.WriteAndClose(_Host); }
                        if(!node.ImageKey.EndsWith("_err")) { node.ImageKey = node.SelectedImageKey = ((node.Parent.Tag is TLibrary) ? "ltest_err" : "test_err"); }
                        break;
                    case TestExecution.ExecutionState.SUCCEEDED:
                        if(running == 0)
                        {
                            if(!ex.Closed) { ((ITestHostWriter) ex.Host).WriteTestResult(ex.Test, true); }
                            ex.WriteAndClose(_Host);
                        }
                        if(!node.ImageKey.EndsWith("_ok")) { node.ImageKey = node.SelectedImageKey = ((node.Parent.Tag is TLibrary) ? "ltest_ok" : "test_ok"); }
                        break;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2022", ex); }
        }


        /// <summary>Updates the tree images and output for running and completed tests.</summary>
        /// <param name="node">Tree node.</param>
        private void _UpdateStep(TreeNode node)
        {
            try
            {
                string prefix = "project";
                bool fail = false;

                if(node.Tag is TLibrary) { prefix = ((TLibrary) node.Tag).LibraryTypeString; }
                string img = prefix + "_wait";

                foreach(TreeNode i in node.Nodes)
                {
                    if(i.Tag is TLibrary) { _UpdateStep(i); }

                    if(i.ImageKey.EndsWith("_run"))
                    {
                        img = prefix + "_run";
                        break;
                    }
                    else if(i.ImageKey.EndsWith("_err"))
                    {
                        img = prefix + "_err";
                        fail = true;
                    }
                    else if(i.ImageKey.EndsWith("_ok"))
                    {
                        if(!fail) { img = prefix + "_ok"; }
                    }
                }

                node.ImageKey = node.SelectedImageKey = img;

                if(node.Tag is TProject)
                {
                    if(img.EndsWith("_ok"))
                    {
                        ((ITestHostWriter) _Host).WriteResult(true);
                    }
                    else if(img.EndsWith("_err")) { ((ITestHostWriter) _Host).WriteResult(false); }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2023", ex); }
        }


        /// <summary>Aborts test execution.</summary>
        private void _Abort(TreeNode node = null)
        {
            try
            {
                if(node == null) { node = _RunningNode; }

                if(node.Tag is IUnitTest)
                {
                    if(((IUnitTest) node.Tag).Data is TestExecution)
                    {
                        while(((TestExecution) ((IUnitTest) node.Tag).Data).State == TestExecution.ExecutionState.EXECUTING)
                        {
                            Application.DoEvents();
                        }
                    }
                }

                foreach(TreeNode i in node.Nodes) { _Abort(i); }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2024", ex); }
        }


        /// <summary>Sets images for a subtree.</summary>
        /// <param name="node">Top node.</param>
        /// <param name="postfix">Image postfix.</param>
        private void _SetSubtreeImages(TreeNode node, string postfix)
        {
            try
            {
                node.ImageKey = node.SelectedImageKey = (node.ImageKey.Split('_')[0] + postfix);

                foreach(TreeNode i in node.Nodes) { _SetSubtreeImages(i, postfix); }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2025", ex); }
        }


        /// <summary>Updates recent files list.</summary>
        private void _UpdateRecentFiles()
        {
            try
            {
                _MenuRecent.DropDownItems.Clear();

                foreach(string i in _RecentFiles)
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(Path.GetFileName(i), _IlistTree.Images["project"], new EventHandler(_MenuRecent_Click));
                    m.Tag = i;
                    m.ToolTipText = i;

                    if(TProject.Current != null) { m.Enabled = (Path.GetFullPath(i) != Path.GetFullPath(TProject.Current.FileName)); }

                    _MenuRecent.DropDownItems.Add(m);
                }

                _MenuRecent.Enabled = (_MenuRecent.DropDownItems.Count > 0);
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSTDO02109", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu "New" click.</summary>
        private void _MenuNew_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                if(_CommitClose())
                {
                    TProject.Current = new TProject();

                    _DrawProject();
                    _SplitMain.Visible = true;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2026", ex); }
        }


        /// <summary>Recent file click.</summary>
        private void _MenuRecent_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(!_CommitClose()) return;

                TProject.Current = TProject.Load((string) ((ToolStripMenuItem) sender).Tag);
                _DrawProject();

                _RecentFiles.Add((string) ((ToolStripMenuItem) sender).Tag);
                _UpdateRecentFiles();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSTDO02112", ex); }
        }


        /// <summary>Menu "Add library" click.</summary>
        private void _MenuAddLibrary_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(TProject.Current == null) return;

                OpenFileDialog f = new OpenFileDialog();
                f.Filter = "utstdo::udiag.filter.libraries".Localize("Test Liberaries") + " (*.exe, *.dll, *.jar)|*.exe;*.dll;*.jar|" + "utstdo::udiag.filter.all".Localize("All Files") + "|*.*";

                if(f.ShowDialog() == DialogResult.OK)
                {
                    TLibrary lib = TLibrary.Load(f.FileName);
                    TProject.Current.Items.Add(lib);

                    TreeNode m = _NodeFromLibrary(lib);

                    _InsertNode(m);
                    _Resort();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2027", ex); }
        }


        /// <summary>Button "Add Test" click.</summary>
        private void _MenuAddTest_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(TProject.Current == null) return;

                OpenFileDialog f = new OpenFileDialog();
                f.Filter = "utstdo::udiag.filter.libraries".Localize("Test Liberaries") + " (*.exe, *.dll, *.jar)|*.exe;*.dll;*.jar|" + "utstdo::udiag.filter.all".Localize("All Files") + "|*.*";

                if(f.ShowDialog() != DialogResult.OK) return;

                TLibrary lib = TLibrary.Load(f.FileName);
                if(lib.Count == 0)
                {
                    string mtext = "udiag::open.notests.text".Localize("The file does not contain any unit tests.");
                    string mcaption = "udiag::open.notests.caption".Localize("Add Test");
                    MessageBox.Show(mtext, mcaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                FormSelectTest s = new FormSelectTest(lib);

                if(s.ShowDialog() == DialogResult.OK)
                {
                    foreach(IUnitTest i in s.Selection)
                    {
                        TProject.Current.Items.Add(i);
                        _InsertNode(_NodeFromTest(i));
                    }
                    _Resort();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2028", ex); }
        }


        /// <summary>Menu "Save as" click.</summary>
        private void _MenuSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                if(TProject.Current != null)
                {
                    _SaveAs();
                    _TreeProject.Nodes[0].Text = TProject.Current.Name;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2029", ex); }
        }


        /// <summary>Menu "Save" click.</summary>
        private void _MenuSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                if(TProject.Current != null)
                {
                    _Save();
                    _TreeProject.Nodes[0].Text = TProject.Current.Name;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2030", ex); }
        }


        /// <summary>Menu "Close" click.</summary>
        private void _MenuClose_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                if(_CommitClose())
                {
                    TProject.Current = null;

                    _SplitMain.Visible = false;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2031", ex); }
        }


        /// <summary>Menu "Open" click.</summary>
        private void _MenuOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(!_CommitClose()) return;

                OpenFileDialog f = new OpenFileDialog();
                f.Filter = "utstdo::udiag.filter.uttxp".Localize("Test Execution Plans") + " (*.uttxp)|*.uttxp|" + "utstdo::udiag.filter.all".Localize("All Files") + "|*.*";

                if(f.ShowDialog() != DialogResult.OK) return;

                TProject.Current = TProject.Load(f.FileName);

                _RecentFiles.Add(f.FileName);
                _UpdateRecentFiles();

                _DrawProject();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2032", ex); }
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                Close();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2033", ex); }
        }


        /// <summary>Form closing.</summary>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if(_Running)
                {
                    e.Cancel = true;
                    return;
                }

                e.Cancel = (!_CommitClose());
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2034", ex); }
        }


        /// <summary>Menu "Delete" click.</summary>
        private void _MenuDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                TreeNode n = _TreeProject.SelectedNode;

                if(n == null) return;
                if(n.Parent != _TreeProject.Nodes[0]) return;

                string mtext = "utstdo::udiag.delete.text".Localize("Do you really want to remove this from the project?");
                string mcaption = "utstdo::udiag.delete.caption".Localize("Delete");
                if(MessageBox.Show(mtext, mcaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    TProject.Current.Items.RemoveAt(((IUnitTest) n.Tag).SortIndex);

                    _TreeProject.SelectedNode = ((n.PrevNode == null) ? n.Parent : n.PrevNode);
                    n.Remove();

                    _Resort();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2035", ex); }
        }


        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                FormAbout f = new FormAbout();
                f.ShowDialog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2036", ex); }
        }


        /// <summary>Menu "Move up" click.</summary>
        private void _MenuMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                TreeNode n = _TreeProject.SelectedNode;

                if(n == null) return;
                if(n == _TreeProject.Nodes[0]) return;
                if(n.Index == 0) return;

                TreeNode d = n.PrevNode;
                d.Remove();
                n.Parent.Nodes.Insert(n.Index + 1, d);

                _Resort();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2037", ex); }
        }


        /// <summary>Menu "Move down" click.</summary>
        private void _MenuMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                TreeNode n = _TreeProject.SelectedNode;

                if(n == null) return;
                if(n == _TreeProject.Nodes[0]) return;
                if(n.NextNode == null) return;

                TreeNode d = n.NextNode;
                d.Remove();
                n.Parent.Nodes.Insert(n.Index, d);

                _Resort();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2038", ex); }
        }


        /// <summary>Menu "Properties" click.</summary>
        private void _MenuProperties_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                if(_TreeProject.SelectedNode == null) return;

                if(_TreeProject.SelectedNode.Tag is TProject)
                {
                    FormProjectProperties f = new FormProjectProperties();
                    f.ShowDialog();
                    _TreeProject.Nodes[0].Text = TProject.Current.Name;
                }
                else if(_TreeProject.SelectedNode.Tag is TLibrary)
                {
                    FormLibraryProperties f = new FormLibraryProperties((TLibrary) _TreeProject.SelectedNode.Tag, _IlistTree.Images[_TreeProject.SelectedNode.ImageKey.Split('_')[0]]);
                    f.ShowDialog();
                }
                else if(_TreeProject.SelectedNode.Tag is IUnitTest)
                {
                    FormTestProperties f = new FormTestProperties((IUnitTest)_TreeProject.SelectedNode.Tag, _IlistTree.Images[_TreeProject.SelectedNode.ImageKey.Split('_')[0]]);
                    f.ShowDialog();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2039", ex); }
        }


        /// <summary>Menu "Run" click.</summary>
        private void _MenuRun_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(TProject.Current == null) return;

                _Run(_TreeProject.Nodes[0]);
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2040", ex); }
        }


        /// <summary>Menu "Run Step" click.</summary>
        private void _MenuRunStep_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(_TreeProject.SelectedNode == null) return;

                _Run(_TreeProject.SelectedNode);
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2041", ex); }
        }


        /// <summary>Host updated.</summary>
        private void _Host_Updated(object sender, EventArgs e)
        {
            try
            {
                _BrowserOut.DocumentText = _Host.HTML;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2042", ex); }
        }


        /// <summary>Menu "Clear output" click.</summary>
        private void _MenuClear_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                _SetSubtreeImages(_TreeProject.Nodes[0], "");
                _Host.Clear();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2043", ex); }
        }


        /// <summary>Menu "Show help" click.</summary>
        private void _MenuShowHelp_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                Process.Start("http://robbiblubber.lima-city.de/wiki.php?title=Help-UTStdo");
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2044", ex); }
        }


        /// <summary>Mouse down event.</summary>
        private void _TreeProject_MouseDown(object sender, MouseEventArgs e)
        {
            _TreeProject.SelectedNode = _TreeProject.HitTest(e.Location).Node;
        }


        /// <summary>Mouse move event.</summary>
        private void _TreeProject_MouseMove(object sender, MouseEventArgs e)
        {
            if((e.Button == MouseButtons.None) || (_TreeProject.SelectedNode == null)) return;

            if(_TreeProject.SelectedNode.Tag is TProject)
            {
                _TreeProject.DoDragDrop(_TreeProject.SelectedNode, DragDropEffects.None);
            }
            else
            {
                _AllowDrop = true;
                _DragNode = _TreeProject.SelectedNode;
                _TreeProject.DoDragDrop(_TreeProject.SelectedNode, DragDropEffects.Move);
            }
        }
        

        /// <summary>Drag over event.</summary>
        private void _TreeProject_DragOver(object sender, DragEventArgs e)
        {
            _DropTarget = _TreeProject.GetNodeAt(_TreeProject.PointToClient(new Point(e.X, e.Y)));
            
            if((!_AllowDrop) || (_DropTarget == null))
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if(_DropTarget == _DragNode)
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            
            if(((_DropTarget.Parent == _DragNode.Parent) && (_DropTarget.Index != (_DragNode.Index - 1)) || (_DropTarget == _DragNode.Parent)))
            {
                e.Effect = DragDropEffects.Move;
                return;
            }

            e.Effect = DragDropEffects.None;
        }


        /// <summary>Drag enter event.</summary>
        private void _TreeProject_DragEnter(object sender, DragEventArgs e)
        {
            _AllowDrop = (e.Data.GetData(typeof(TreeNode)) == _DragNode);
        }


        /// <summary>Drag drop event.</summary>
        private void _TreeProject_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if(_DropTarget == _DragNode.Parent)
                {
                    _DragNode.Remove();
                    _DropTarget.Nodes.Insert(0, _DragNode);
                    _TreeProject.SelectedNode = _DragNode;

                    _DropTarget = _DragNode = null;

                    _Resort();
                }
                else if(_DropTarget.Parent == _DragNode.Parent)
                {
                    _DragNode.Remove();
                    _DropTarget.Parent.Nodes.Insert(_DropTarget.Index + 1, _DragNode);
                    _TreeProject.SelectedNode = _DragNode;

                    _DropTarget = _DragNode = null;

                    _Resort();
                }
            }
            catch(Exception ex) { DebugOp.Dump("UTSDO2045", ex); }
        }


        /// <summary>Menu "Export" click.</summary>
        private void _MenuExport_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;

                SaveFileDialog f = new SaveFileDialog();
                f.Filter = "utstdo::udiag.filter.html".Localize("HTML Documents") + " (*.html)|*.html|" + "utstdo::udiag.filter.all".Localize("All Files") + "|*.*";

                if(f.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(f.FileName, _BrowserOut.DocumentText);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2046", ex); }
        }


        /// <summary>Context menu opening.</summary>
        private void _CmenuTree_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if(_TreeProject.SelectedNode == null)
                {
                    e.Cancel = true;
                    return;
                }

                _CmenuAdd.Visible = (_TreeProject.SelectedNode.Tag is TProject);
                _CmenuDelete.Visible = ((_TreeProject.SelectedNode.Parent == null) ? false : (_TreeProject.SelectedNode.Parent.Tag is TProject));

                if(_TreeProject.SelectedNode.Tag is TProject)
                {
                    _CmenuMove.Visible = false;
                }
                else
                {
                    _CmenuMove.Visible = (_TreeProject.SelectedNode.Parent.Nodes.Count > 1);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2047", ex); }
        }


        /// <summary>Menu "File" opening.</summary>
        private void _MenuFile_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if(_Running)
                {
                    _MenuNew.Enabled = _MenuOpen.Enabled = false;
                    _MenuClose.Enabled = _MenuAdd.Enabled = _MenuSave.Enabled = _MenuSaveAs.Enabled = _MenuExport.Enabled = false;
                    _MenuExit.Enabled = false;
                }
                else
                {
                    _MenuNew.Enabled = _MenuOpen.Enabled = true;
                    _MenuClose.Enabled = _MenuAdd.Enabled = _MenuSave.Enabled = _MenuSaveAs.Enabled = _MenuExport.Enabled = (TProject.Current != null);
                    _MenuExit.Enabled = true;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2048", ex); }
        }


        /// <summary>Menu "File" closed.</summary>
        private void _MenuFile_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                _MenuNew.Enabled = _MenuOpen.Enabled = true;
                _MenuClose.Enabled = _MenuAdd.Enabled = _MenuSave.Enabled = _MenuSaveAs.Enabled = _MenuExport.Enabled = true;
                _MenuExit.Enabled = true;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2049", ex); }
        }


        /// <summary>Menu "Move" opening.</summary>
        private void _MenuMove_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if(_TreeProject.SelectedNode == null)
                {
                    _MenuMoveUp.Enabled = _MenuMoveDown.Enabled = false;
                    return;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2050", ex); }

            try
            {
                _MenuMoveUp.Enabled = (_TreeProject.SelectedNode.Index > 0);
                _MenuMoveDown.Enabled = (_TreeProject.SelectedNode.Index < (_TreeProject.SelectedNode.Parent.Nodes.Count - 1));
            }
            catch(Exception ex) { DebugOp.Dump("UTSTDO00400", ex); }
        }


        /// <summary>Menu "Move" closed.</summary>
        private void _MenuMove_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                _MenuMoveUp.Enabled = _MenuMoveDown.Enabled = true;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2051", ex); }
        }


        /// <summary>Context menu "Move" opening.</summary>
        private void _CmenuMove_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if(_TreeProject.SelectedNode == null)
                {
                    _MenuMoveUp.Enabled = _MenuMoveDown.Enabled = false;
                    return;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2052", ex); }

            try
            {
                _CmenuMoveUp.Enabled = (_TreeProject.SelectedNode.Index > 0);
                _CmenuMoveDown.Enabled = (_TreeProject.SelectedNode.Index < (_TreeProject.SelectedNode.Parent.Nodes.Count - 1));
            }
            catch(Exception ex) { DebugOp.Dump("UTSTDO00401", ex); }
        }


        /// <summary>Menu "Edit" opening.</summary>
        private void editToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if(_Running)
                {
                    _MenuMove.Enabled = _MenuDelete.Enabled = _MenuProperties.Enabled = false;
                }
                else
                {
                    if(_TreeProject.SelectedNode == null)
                    {
                        _MenuMove.Enabled = _MenuDelete.Enabled = _MenuProperties.Enabled = false;
                    }
                    else if(_TreeProject.SelectedNode.Tag is TProject)
                    {
                        _MenuMove.Enabled = _MenuDelete.Enabled = false;
                        _MenuProperties.Enabled = true;
                    }
                    else
                    {
                        _MenuMove.Enabled = (_TreeProject.SelectedNode.Parent.Nodes.Count > 1);
                        _MenuDelete.Enabled = (_TreeProject.SelectedNode.Parent.Tag is TProject);
                        _MenuProperties.Enabled = true;
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2053", ex); }
        }


        /// <summary>Menu "Edit" closed.</summary>
        private void editToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                _MenuMove.Enabled = _MenuDelete.Enabled = _MenuProperties.Enabled = true;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2054", ex); }
        }


        /// <summary>Menu "Test" opening.</summary>
        private void _MenuTest_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if(_Running)
                {
                    _MenuRun.Visible = _MenuRunStep.Visible = false;
                    _MenuAbort.Visible = true;
                    _MenuAbort.Enabled = (!_Aborting);
                    _MenuClear.Enabled = false;
                }
                else
                {
                    _MenuRun.Visible = _MenuRunStep.Visible = true;
                    _MenuAbort.Visible = false;

                    if(TProject.Current == null)
                    {
                        _MenuRun.Enabled = _MenuRunStep.Enabled = _MenuClear.Enabled = false;
                    }
                    else
                    {
                        _MenuRun.Enabled = true;
                        _MenuRunStep.Enabled = (!((_TreeProject.SelectedNode == null) || (_TreeProject.SelectedNode.Tag is TProject)));
                        _MenuClear.Enabled = true;
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2055", ex); }
        }


        /// <summary>Menu "Test" closed.</summary>
        private void _MenuTest_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                _MenuRun.Enabled = _MenuRunStep.Enabled = _MenuClear.Enabled = true;
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2056", ex); }
        }


        /// <summary>Menu "Start/Stop Logging" click.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            if(_Running) return;

            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save Dump" click.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(!DebugOp.Enabled) return;

                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "debugx::udiag.filter.dump".Localize("Debug Dump Files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    DebugOp.CreateDumpFile(d.FileName);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2057", ex); }
        }


        /// <summary>Menu "View Dump" click.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(!DebugOp.Enabled) return;

                DebugOp.ShowLog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2058", ex); }
        }


        /// <summary>Menu "Upload Dump" click.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Running) return;
                if(!DebugOp.Enabled) return;

                DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/Robbiblubber.Util.NET/");
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2059", ex); }
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = _MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true;
        }


        /// <summary>Menu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            if(_Running) return;

            FormSettings f = new FormSettings();
            f.ShowDialog();
        }


        /// <summary>Run timer tick.</summary>
        private void _TimerRun_Tick(object sender, EventArgs e)
        {
            try
            {
                _TimerRun.Enabled = false;

                if(_Aborting)
                {
                    _SetSubtreeImages(_RunningNode, "_stop");
                    _Abort();
                    _SetSubtreeImages(_RunningNode, "");

                    _Host.Clear();

                    _Aborting = _Running = false;
                    _PanelRun.Visible = false;

                    return;
                }
                Application.DoEvents();

                int running = 0;
                bool syncWait = (!TProject.Current.AllowParallel);

                _RunStep(_RunningNode, ref running, ref syncWait);
                _UpdateStep(_RunningNode);

                if(running == 0)
                {
                    _Running = false;
                    _PanelRun.Visible = false;
                }
                else { _TimerRun.Enabled = true; }
            }
            catch(Exception ex) { DebugOp.DumpMessage("UTSDO2060", ex); }
        }


        /// <summary>Menu "Abort" click.</summary>
        private void _MenuAbort_Click(object sender, EventArgs e)
        {
            if(!_Running) return;

            _Aborting = true;
        }


        /// <summary>Menu "Tools" opening.</summary>
        private void _MenuTools_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebug.Enabled = _MenuSettings.Enabled = (!_Running);
        }


        /// <summary>Menu "Tools" closed.</summary>
        private void _MenuTools_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebug.Enabled = _MenuSettings.Enabled = true;
        }


        /// <summary>Menu "Help" opening.</summary>
        private void _MenuHelp_DropDownOpening(object sender, EventArgs e)
        {
            _MenuShowHelp.Enabled = _MenuAbout.Enabled = (!_Running);
        }


        /// <summary>Menu "Help" closed.</summary>
        private void _MenuHelp_DropDownClosed(object sender, EventArgs e)
        {
            _MenuShowHelp.Enabled = _MenuAbout.Enabled = true;
        }
    }
}
