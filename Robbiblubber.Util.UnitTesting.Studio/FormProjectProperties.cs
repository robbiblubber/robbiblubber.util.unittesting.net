﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Util.UnitTesting;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>This class implements the project properties window.</summary>
    public partial class FormProjectProperties: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormProjectProperties()
        {
            InitializeComponent();

            this.Localize();

            _TextName.Text = TProject.Current.Name;
            _CheckParallel.Checked = TProject.Current.AllowParallel;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            TProject.Current.Name = _TextName.Text;
            TProject.Current.AllowParallel = _CheckParallel.Checked;
        }
    }
}
