﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Util.UnitTesting;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>This class implements the test properties window.</summary>
    public partial class FormTestProperties: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="test">Unit test.</param>
        /// <param name="img">Image.</param>
        public FormTestProperties(IUnitTest test, Image img)
        {
            InitializeComponent();

            this.Localize();

            Icon = Icon.FromHandle(((Bitmap) img).GetHicon());

            _LabelName.Text = test.Name;
            _LabelDescription.Text = test.Description;

            _TextFileName.Text = test.FileName;
            _TextClass.Text = test.ClassName;

            _LabelParallel.Text = (test.Parallelizable ? "utstdo::udiag.testp.yes".Localize("Yes") : "utstdo::udiag.testp.no".Localize("No"));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
