﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Util.UnitTesting;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>Test selection window.</summary>
    public partial class FormSelectTest: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="lib">Libarary.</param>
        public FormSelectTest(TLibrary lib)
        {
            InitializeComponent();

            this.Localize();

            foreach(IUnitTest i in lib)
            {
                ListViewItem m = new ListViewItem(i.Name, "test");
                m.ToolTipText = i.Description;
                m.Tag = i;
                m.Checked = true;
                
                _ListTest.Items.Add(m);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the selected tests.</summary>
        public List<IUnitTest> Selection
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item checked.</summary>
        private void _ListTest_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            foreach(ListViewItem i in _ListTest.Items)
            {
                if(i.Checked)
                {
                    _ButtonOK.Enabled = true;
                    return;
                }
            }

            _ButtonOK.Enabled = false;
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            Selection = new List<IUnitTest>();

            foreach(ListViewItem i in _ListTest.Items)
            {
                if(i.Checked) { Selection.Add((IUnitTest) i.Tag); }
            }
            Close();
        }
    }
}
