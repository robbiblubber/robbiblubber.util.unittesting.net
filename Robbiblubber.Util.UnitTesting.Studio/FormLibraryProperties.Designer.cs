﻿namespace Robbiblubber.Util.UnitTesting.Studio
{
    partial class FormLibraryProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLibraryProperties));
            this._ButtonClose = new System.Windows.Forms.Button();
            this._LabelFileNameCaption = new System.Windows.Forms.Label();
            this._LabelVersion = new System.Windows.Forms.Label();
            this._LabelVersionCaption = new System.Windows.Forms.Label();
            this._LabelTestCount = new System.Windows.Forms.Label();
            this._LabelTestCountCaption = new System.Windows.Forms.Label();
            this._TextFileName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Location = new System.Drawing.Point(445, 135);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(125, 28);
            this._ButtonClose.TabIndex = 0;
            this._ButtonClose.Tag = "utstdo::common.button.close";
            this._ButtonClose.Text = "&Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            // 
            // _LabelFileNameCaption
            // 
            this._LabelFileNameCaption.AutoSize = true;
            this._LabelFileNameCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFileNameCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFileNameCaption.Location = new System.Drawing.Point(23, 20);
            this._LabelFileNameCaption.Name = "_LabelFileNameCaption";
            this._LabelFileNameCaption.Size = new System.Drawing.Size(60, 13);
            this._LabelFileNameCaption.TabIndex = 5;
            this._LabelFileNameCaption.Tag = "utstdo::udiag.libp.filename";
            this._LabelFileNameCaption.Text = "File Name:";
            // 
            // _LabelVersion
            // 
            this._LabelVersion.Location = new System.Drawing.Point(23, 84);
            this._LabelVersion.Name = "_LabelVersion";
            this._LabelVersion.Size = new System.Drawing.Size(389, 22);
            this._LabelVersion.TabIndex = 8;
            this._LabelVersion.Text = "---";
            // 
            // _LabelVersionCaption
            // 
            this._LabelVersionCaption.AutoSize = true;
            this._LabelVersionCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelVersionCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelVersionCaption.Location = new System.Drawing.Point(23, 67);
            this._LabelVersionCaption.Name = "_LabelVersionCaption";
            this._LabelVersionCaption.Size = new System.Drawing.Size(48, 13);
            this._LabelVersionCaption.TabIndex = 7;
            this._LabelVersionCaption.Tag = "utstdo::udiag.libp.version";
            this._LabelVersionCaption.Text = "Version:";
            // 
            // _LabelTestCount
            // 
            this._LabelTestCount.Location = new System.Drawing.Point(442, 84);
            this._LabelTestCount.Name = "_LabelTestCount";
            this._LabelTestCount.Size = new System.Drawing.Size(128, 22);
            this._LabelTestCount.TabIndex = 10;
            this._LabelTestCount.Text = "---";
            // 
            // _LabelTestCountCaption
            // 
            this._LabelTestCountCaption.AutoSize = true;
            this._LabelTestCountCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTestCountCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTestCountCaption.Location = new System.Drawing.Point(442, 67);
            this._LabelTestCountCaption.Name = "_LabelTestCountCaption";
            this._LabelTestCountCaption.Size = new System.Drawing.Size(34, 13);
            this._LabelTestCountCaption.TabIndex = 9;
            this._LabelTestCountCaption.Tag = "utstdo::udiag.libp.tests";
            this._LabelTestCountCaption.Text = "Tests:";
            // 
            // _TextFileName
            // 
            this._TextFileName.BackColor = System.Drawing.SystemColors.Control;
            this._TextFileName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._TextFileName.Location = new System.Drawing.Point(26, 36);
            this._TextFileName.Name = "_TextFileName";
            this._TextFileName.Size = new System.Drawing.Size(544, 18);
            this._TextFileName.TabIndex = 11;
            // 
            // FormLibraryProperties
            // 
            this.AcceptButton = this._ButtonClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(605, 188);
            this.Controls.Add(this._TextFileName);
            this.Controls.Add(this._LabelTestCount);
            this.Controls.Add(this._LabelTestCountCaption);
            this.Controls.Add(this._LabelVersion);
            this.Controls.Add(this._LabelVersionCaption);
            this.Controls.Add(this._LabelFileNameCaption);
            this.Controls.Add(this._ButtonClose);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLibraryProperties";
            this.Tag = "utstdo::udiag.libp.caption";
            this.Text = "Properties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.Label _LabelFileNameCaption;
        private System.Windows.Forms.Label _LabelVersion;
        private System.Windows.Forms.Label _LabelVersionCaption;
        private System.Windows.Forms.Label _LabelTestCount;
        private System.Windows.Forms.Label _LabelTestCountCaption;
        private System.Windows.Forms.TextBox _TextFileName;
    }
}