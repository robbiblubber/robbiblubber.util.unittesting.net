﻿namespace Robbiblubber.Util.UnitTesting.Studio
{
    partial class FormTestProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTestProperties));
            this._ButtonClose = new System.Windows.Forms.Button();
            this._LabelFileNameCaption = new System.Windows.Forms.Label();
            this._TextFileName = new System.Windows.Forms.TextBox();
            this._TextClass = new System.Windows.Forms.TextBox();
            this._LabelClassCaption = new System.Windows.Forms.Label();
            this._LabelNameCaption = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._LabelDescriptionCaption = new System.Windows.Forms.Label();
            this._LabelParallelCaption = new System.Windows.Forms.Label();
            this._LabelParallel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Location = new System.Drawing.Point(445, 291);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(125, 28);
            this._ButtonClose.TabIndex = 0;
            this._ButtonClose.Tag = "utstdo::common.button.close";
            this._ButtonClose.Text = "&Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            // 
            // _LabelFileNameCaption
            // 
            this._LabelFileNameCaption.AutoSize = true;
            this._LabelFileNameCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFileNameCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFileNameCaption.Location = new System.Drawing.Point(23, 187);
            this._LabelFileNameCaption.Name = "_LabelFileNameCaption";
            this._LabelFileNameCaption.Size = new System.Drawing.Size(28, 13);
            this._LabelFileNameCaption.TabIndex = 2;
            this._LabelFileNameCaption.Tag = "utstdo::udiag.testp.file";
            this._LabelFileNameCaption.Text = "File:";
            // 
            // _TextFileName
            // 
            this._TextFileName.BackColor = System.Drawing.SystemColors.Control;
            this._TextFileName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._TextFileName.Location = new System.Drawing.Point(26, 203);
            this._TextFileName.Name = "_TextFileName";
            this._TextFileName.ReadOnly = true;
            this._TextFileName.Size = new System.Drawing.Size(544, 18);
            this._TextFileName.TabIndex = 2;
            // 
            // _TextClass
            // 
            this._TextClass.BackColor = System.Drawing.SystemColors.Control;
            this._TextClass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._TextClass.Location = new System.Drawing.Point(26, 250);
            this._TextClass.Name = "_TextClass";
            this._TextClass.ReadOnly = true;
            this._TextClass.Size = new System.Drawing.Size(544, 18);
            this._TextClass.TabIndex = 3;
            // 
            // _LabelClassCaption
            // 
            this._LabelClassCaption.AutoSize = true;
            this._LabelClassCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelClassCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelClassCaption.Location = new System.Drawing.Point(23, 234);
            this._LabelClassCaption.Name = "_LabelClassCaption";
            this._LabelClassCaption.Size = new System.Drawing.Size(36, 13);
            this._LabelClassCaption.TabIndex = 3;
            this._LabelClassCaption.Tag = "utstdo::udiag.testp.class";
            this._LabelClassCaption.Text = "Class:";
            // 
            // _LabelNameCaption
            // 
            this._LabelNameCaption.AutoSize = true;
            this._LabelNameCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameCaption.Location = new System.Drawing.Point(23, 29);
            this._LabelNameCaption.Name = "_LabelNameCaption";
            this._LabelNameCaption.Size = new System.Drawing.Size(61, 13);
            this._LabelNameCaption.TabIndex = 0;
            this._LabelNameCaption.Tag = "utstdo::udiag.testp.name";
            this._LabelNameCaption.Text = "Test Name:";
            // 
            // _LabelName
            // 
            this._LabelName.Location = new System.Drawing.Point(23, 42);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(555, 21);
            this._LabelName.TabIndex = 0;
            // 
            // _LabelDescription
            // 
            this._LabelDescription.Location = new System.Drawing.Point(23, 87);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(555, 90);
            this._LabelDescription.TabIndex = 1;
            // 
            // _LabelDescriptionCaption
            // 
            this._LabelDescriptionCaption.AutoSize = true;
            this._LabelDescriptionCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescriptionCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescriptionCaption.Location = new System.Drawing.Point(23, 74);
            this._LabelDescriptionCaption.Name = "_LabelDescriptionCaption";
            this._LabelDescriptionCaption.Size = new System.Drawing.Size(69, 13);
            this._LabelDescriptionCaption.TabIndex = 1;
            this._LabelDescriptionCaption.Tag = "utstdo::udiag.testp.description";
            this._LabelDescriptionCaption.Text = "Description:";
            // 
            // _LabelParallelCaption
            // 
            this._LabelParallelCaption.AutoSize = true;
            this._LabelParallelCaption.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelParallelCaption.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelParallelCaption.Location = new System.Drawing.Point(23, 300);
            this._LabelParallelCaption.Name = "_LabelParallelCaption";
            this._LabelParallelCaption.Size = new System.Drawing.Size(77, 13);
            this._LabelParallelCaption.TabIndex = 4;
            this._LabelParallelCaption.Tag = "utstdo::udiag.testp.parallel";
            this._LabelParallelCaption.Text = "Parallelizable:";
            // 
            // _LabelParallel
            // 
            this._LabelParallel.AutoSize = true;
            this._LabelParallel.Location = new System.Drawing.Point(106, 296);
            this._LabelParallel.Name = "_LabelParallel";
            this._LabelParallel.Size = new System.Drawing.Size(27, 17);
            this._LabelParallel.TabIndex = 4;
            this._LabelParallel.Text = "Yes";
            // 
            // FormTestProperties
            // 
            this.AcceptButton = this._ButtonClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(605, 340);
            this.Controls.Add(this._LabelParallel);
            this.Controls.Add(this._LabelParallelCaption);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._LabelDescriptionCaption);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._LabelNameCaption);
            this.Controls.Add(this._TextClass);
            this.Controls.Add(this._LabelClassCaption);
            this.Controls.Add(this._TextFileName);
            this.Controls.Add(this._LabelFileNameCaption);
            this.Controls.Add(this._ButtonClose);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTestProperties";
            this.Tag = "utstdo::udiag.testp.caption";
            this.Text = "Properties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.Label _LabelFileNameCaption;
        private System.Windows.Forms.TextBox _TextFileName;
        private System.Windows.Forms.TextBox _TextClass;
        private System.Windows.Forms.Label _LabelClassCaption;
        private System.Windows.Forms.Label _LabelNameCaption;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.Label _LabelDescriptionCaption;
        private System.Windows.Forms.Label _LabelParallelCaption;
        private System.Windows.Forms.Label _LabelParallel;
    }
}