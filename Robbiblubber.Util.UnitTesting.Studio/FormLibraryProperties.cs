﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Util.UnitTesting;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>This class implements the library properties window.</summary>
    public partial class FormLibraryProperties: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="lib">Library.</param>
        /// <param name="img">Image.</param>
        public FormLibraryProperties(TLibrary lib, Image img)
        {
            InitializeComponent();

            Icon = Icon.FromHandle(((Bitmap) img).GetHicon());

            this.Localize();

            _TextFileName.Text = lib.FileName;
            _LabelVersion.Text = lib.Version.ToVersionString();
            _LabelTestCount.Text = lib.Count.ToString();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
