﻿namespace Robbiblubber.Util.UnitTesting.Studio
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._IlistSettings = new System.Windows.Forms.ImageList(this.components);
            this._CboLevel = new Robbiblubber.Util.Controls.RichComboBox();
            this._LabelLevel = new System.Windows.Forms.Label();
            this._ChkDebug = new System.Windows.Forms.CheckBox();
            this._LcboLanguage = new Robbiblubber.Util.Localization.Controls.LocaleComboBox();
            this._LabelLanguage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(359, 233);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(125, 28);
            this._ButtonCancel.TabIndex = 4;
            this._ButtonCancel.Tag = "utstdo::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(228, 233);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 3;
            this._ButtonOK.Tag = "utstdo::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _IlistSettings
            // 
            this._IlistSettings.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistSettings.ImageStream")));
            this._IlistSettings.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistSettings.Images.SetKeyName(0, "settings");
            this._IlistSettings.Images.SetKeyName(1, "min");
            this._IlistSettings.Images.SetKeyName(2, "default");
            this._IlistSettings.Images.SetKeyName(3, "all");
            // 
            // _CboLevel
            // 
            this._CboLevel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._CboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._CboLevel.ImageList = this._IlistSettings;
            this._CboLevel.Location = new System.Drawing.Point(40, 49);
            this._CboLevel.Name = "_CboLevel";
            this._CboLevel.Size = new System.Drawing.Size(444, 26);
            this._CboLevel.TabIndex = 5;
            // 
            // _LabelLevel
            // 
            this._LabelLevel.AutoSize = true;
            this._LabelLevel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLevel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLevel.Location = new System.Drawing.Point(37, 33);
            this._LabelLevel.Name = "_LabelLevel";
            this._LabelLevel.Size = new System.Drawing.Size(98, 13);
            this._LabelLevel.TabIndex = 6;
            this._LabelLevel.Tag = "utstdo::udiag.settings.level";
            this._LabelLevel.Text = "Output Log &Level:";
            // 
            // _ChkDebug
            // 
            this._ChkDebug.AutoSize = true;
            this._ChkDebug.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._ChkDebug.Location = new System.Drawing.Point(40, 175);
            this._ChkDebug.Name = "_ChkDebug";
            this._ChkDebug.Size = new System.Drawing.Size(372, 21);
            this._ChkDebug.TabIndex = 7;
            this._ChkDebug.Tag = "debugx::settings.showdebug";
            this._ChkDebug.Text = "   Enable &debug features (show Debug menu in menu strip)";
            this._ChkDebug.UseVisualStyleBackColor = true;
            // 
            // _LcboLanguage
            // 
            this._LcboLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LcboLanguage.Location = new System.Drawing.Point(40, 106);
            this._LcboLanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LcboLanguage.Name = "_LcboLanguage";
            this._LcboLanguage.Size = new System.Drawing.Size(444, 31);
            this._LcboLanguage.TabIndex = 9;
            this._LcboLanguage.Value = null;
            // 
            // _LabelLanguage
            // 
            this._LabelLanguage.AutoSize = true;
            this._LabelLanguage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLanguage.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLanguage.Location = new System.Drawing.Point(37, 87);
            this._LabelLanguage.Name = "_LabelLanguage";
            this._LabelLanguage.Size = new System.Drawing.Size(135, 13);
            this._LabelLanguage.TabIndex = 8;
            this._LabelLanguage.Tag = "utstdo::udiag.settings.language";
            this._LabelLanguage.Text = "User Interface &Language:";
            // 
            // FormSettings
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(534, 283);
            this.Controls.Add(this._LcboLanguage);
            this.Controls.Add(this._LabelLanguage);
            this.Controls.Add(this._ChkDebug);
            this.Controls.Add(this._LabelLevel);
            this.Controls.Add(this._CboLevel);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.Tag = "utstdo::udiag.settings.caption";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.ImageList _IlistSettings;
        private Controls.RichComboBox _CboLevel;
        private System.Windows.Forms.Label _LabelLevel;
        private System.Windows.Forms.CheckBox _ChkDebug;
        private Localization.Controls.LocaleComboBox _LcboLanguage;
        private System.Windows.Forms.Label _LabelLanguage;
    }
}