﻿using System;
using System.Threading;
using System.Windows.Forms;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>Program class.</summary>
    internal static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static members                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Splash window.</summary>
        internal static FormSplash __Splash;

        /// <summary>Main window.</summary>
        internal static FormMain __Main;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // main entry point                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="argv">Arguments.</param>
        [STAThread]
        internal static void Main(string[] argv)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            PathOp.ApplicationPart = "robbiblubber.org/utstdo";

            __Splash = new FormSplash();
            __Splash.Show();
            Application.DoEvents();

            for(int i = 0; i < 36; i++)
            {
                Thread.Sleep(36);
                Application.DoEvents();
            }

            string file = null;

            foreach(string i in argv) { file = i; }

            if(file == null)
            {
                __Main = new FormMain();
            }
            else
            {
                __Main = new FormMain(file);
            }

            __Splash.DelayClose();

            Application.Run(__Main);
        }
    }
}
