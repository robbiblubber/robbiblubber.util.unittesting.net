﻿namespace Robbiblubber.Util.UnitTesting.Studio
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this._MenuMain = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNew = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuClose = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuAdd = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddLibrary = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddTest = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank2 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuExport = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank3 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuRecent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMove = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTest = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRun = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRunStep = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbort = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBlankTest0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuClear = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._SplitMain = new System.Windows.Forms.SplitContainer();
            this._TreeProject = new System.Windows.Forms.TreeView();
            this._CmenuTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuTreeBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuAdd = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddLibrary = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddTest = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMove = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuTreeBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this._IlistTree = new System.Windows.Forms.ImageList(this.components);
            this._PanelRun = new System.Windows.Forms.Panel();
            this._LabelRunText = new System.Windows.Forms.Label();
            this._LabelRun = new System.Windows.Forms.Label();
            this._PanelBrowser = new System.Windows.Forms.Panel();
            this._BrowserOut = new System.Windows.Forms.WebBrowser();
            this._TimerRun = new System.Windows.Forms.Timer(this.components);
            this._MenuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._SplitMain)).BeginInit();
            this._SplitMain.Panel1.SuspendLayout();
            this._SplitMain.Panel2.SuspendLayout();
            this._SplitMain.SuspendLayout();
            this._CmenuTree.SuspendLayout();
            this._PanelRun.SuspendLayout();
            this._PanelBrowser.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuMain
            // 
            this._MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this.editToolStripMenuItem,
            this._MenuTest,
            this._MenuTools,
            this._MenuHelp});
            this._MenuMain.Location = new System.Drawing.Point(0, 0);
            this._MenuMain.Name = "_MenuMain";
            this._MenuMain.Size = new System.Drawing.Size(1321, 24);
            this._MenuMain.TabIndex = 0;
            this._MenuMain.Text = "menuStrip1";
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNew,
            this._MenuOpen,
            this._MenuSave,
            this._MenuSaveAs,
            this._MenuFileBlank0,
            this._MenuClose,
            this._MenuFileBlank1,
            this._MenuAdd,
            this._MenuFileBlank2,
            this._MenuExport,
            this._MenuFileBlank3,
            this._MenuRecent,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 20);
            this._MenuFile.Tag = "utstdo::menu.file";
            this._MenuFile.Text = "&File";
            this._MenuFile.DropDownClosed += new System.EventHandler(this._MenuFile_DropDownClosed);
            this._MenuFile.DropDownOpening += new System.EventHandler(this._MenuFile_DropDownOpening);
            // 
            // _MenuNew
            // 
            this._MenuNew.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNew.Image")));
            this._MenuNew.Name = "_MenuNew";
            this._MenuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._MenuNew.Size = new System.Drawing.Size(197, 22);
            this._MenuNew.Tag = "utstdo::menu.new";
            this._MenuNew.Text = "&New";
            this._MenuNew.Click += new System.EventHandler(this._MenuNew_Click);
            // 
            // _MenuOpen
            // 
            this._MenuOpen.Image = ((System.Drawing.Image)(resources.GetObject("_MenuOpen.Image")));
            this._MenuOpen.Name = "_MenuOpen";
            this._MenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._MenuOpen.Size = new System.Drawing.Size(197, 22);
            this._MenuOpen.Tag = "utstdo::menu.open";
            this._MenuOpen.Text = "&Open...";
            this._MenuOpen.Click += new System.EventHandler(this._MenuOpen_Click);
            // 
            // _MenuSave
            // 
            this._MenuSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSave.Image")));
            this._MenuSave.Name = "_MenuSave";
            this._MenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSave.Size = new System.Drawing.Size(197, 22);
            this._MenuSave.Tag = "utstdo::menu.save";
            this._MenuSave.Text = "&Save";
            this._MenuSave.Click += new System.EventHandler(this._MenuSave_Click);
            // 
            // _MenuSaveAs
            // 
            this._MenuSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSaveAs.Image")));
            this._MenuSaveAs.Name = "_MenuSaveAs";
            this._MenuSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this._MenuSaveAs.Size = new System.Drawing.Size(197, 22);
            this._MenuSaveAs.Tag = "utstdo::menu.saveas";
            this._MenuSaveAs.Text = "Save &As...";
            this._MenuSaveAs.Click += new System.EventHandler(this._MenuSaveAs_Click);
            // 
            // _MenuFileBlank0
            // 
            this._MenuFileBlank0.Name = "_MenuFileBlank0";
            this._MenuFileBlank0.Size = new System.Drawing.Size(194, 6);
            // 
            // _MenuClose
            // 
            this._MenuClose.Image = ((System.Drawing.Image)(resources.GetObject("_MenuClose.Image")));
            this._MenuClose.Name = "_MenuClose";
            this._MenuClose.Size = new System.Drawing.Size(197, 22);
            this._MenuClose.Tag = "utstdo::menu.close";
            this._MenuClose.Text = "&Close";
            this._MenuClose.Click += new System.EventHandler(this._MenuClose_Click);
            // 
            // _MenuFileBlank1
            // 
            this._MenuFileBlank1.Name = "_MenuFileBlank1";
            this._MenuFileBlank1.Size = new System.Drawing.Size(194, 6);
            // 
            // _MenuAdd
            // 
            this._MenuAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuAddLibrary,
            this._MenuAddTest});
            this._MenuAdd.Name = "_MenuAdd";
            this._MenuAdd.Size = new System.Drawing.Size(197, 22);
            this._MenuAdd.Tag = "utstdo::menu.add";
            this._MenuAdd.Text = "Ad&d";
            // 
            // _MenuAddLibrary
            // 
            this._MenuAddLibrary.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddLibrary.Image")));
            this._MenuAddLibrary.Name = "_MenuAddLibrary";
            this._MenuAddLibrary.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.L)));
            this._MenuAddLibrary.Size = new System.Drawing.Size(191, 22);
            this._MenuAddLibrary.Tag = "utstdo::menu.addlibrary";
            this._MenuAddLibrary.Text = "Library...";
            this._MenuAddLibrary.Click += new System.EventHandler(this._MenuAddLibrary_Click);
            // 
            // _MenuAddTest
            // 
            this._MenuAddTest.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddTest.Image")));
            this._MenuAddTest.Name = "_MenuAddTest";
            this._MenuAddTest.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.T)));
            this._MenuAddTest.Size = new System.Drawing.Size(191, 22);
            this._MenuAddTest.Tag = "utstdo::menu.addtest";
            this._MenuAddTest.Text = "&Test...";
            this._MenuAddTest.Click += new System.EventHandler(this._MenuAddTest_Click);
            // 
            // _MenuFileBlank2
            // 
            this._MenuFileBlank2.Name = "_MenuFileBlank2";
            this._MenuFileBlank2.Size = new System.Drawing.Size(194, 6);
            // 
            // _MenuExport
            // 
            this._MenuExport.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExport.Image")));
            this._MenuExport.Name = "_MenuExport";
            this._MenuExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this._MenuExport.Size = new System.Drawing.Size(197, 22);
            this._MenuExport.Tag = "utstdo::menu.export";
            this._MenuExport.Text = "&Export Output...";
            this._MenuExport.Click += new System.EventHandler(this._MenuExport_Click);
            // 
            // _MenuFileBlank3
            // 
            this._MenuFileBlank3.Name = "_MenuFileBlank3";
            this._MenuFileBlank3.Size = new System.Drawing.Size(194, 6);
            // 
            // _MenuRecent
            // 
            this._MenuRecent.Name = "_MenuRecent";
            this._MenuRecent.Size = new System.Drawing.Size(197, 22);
            this._MenuRecent.Text = "Recent Files";
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._MenuExit.Size = new System.Drawing.Size(197, 22);
            this._MenuExit.Tag = "utstdo::menu.exit";
            this._MenuExit.Text = "E&xit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuMove,
            this._MenuDelete,
            this.toolStripMenuItem2,
            this._MenuProperties});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Tag = "utstdo::menu.edit";
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.DropDownClosed += new System.EventHandler(this.editToolStripMenuItem_DropDownClosed);
            this.editToolStripMenuItem.DropDownOpening += new System.EventHandler(this.editToolStripMenuItem_DropDownOpening);
            // 
            // _MenuMove
            // 
            this._MenuMove.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuMoveUp,
            this._MenuMoveDown});
            this._MenuMove.Name = "_MenuMove";
            this._MenuMove.Size = new System.Drawing.Size(181, 22);
            this._MenuMove.Tag = "utstdo::menu.move";
            this._MenuMove.Text = "&Move";
            this._MenuMove.DropDownClosed += new System.EventHandler(this._MenuMove_DropDownClosed);
            this._MenuMove.DropDownOpening += new System.EventHandler(this._MenuMove_DropDownOpening);
            // 
            // _MenuMoveUp
            // 
            this._MenuMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuMoveUp.Image")));
            this._MenuMoveUp.Name = "_MenuMoveUp";
            this._MenuMoveUp.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Up)));
            this._MenuMoveUp.Size = new System.Drawing.Size(170, 22);
            this._MenuMoveUp.Tag = "utstdo::menu.moveup";
            this._MenuMoveUp.Text = "&Up";
            this._MenuMoveUp.Click += new System.EventHandler(this._MenuMoveUp_Click);
            // 
            // _MenuMoveDown
            // 
            this._MenuMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("_MenuMoveDown.Image")));
            this._MenuMoveDown.Name = "_MenuMoveDown";
            this._MenuMoveDown.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Down)));
            this._MenuMoveDown.Size = new System.Drawing.Size(170, 22);
            this._MenuMoveDown.Tag = "utstdo::menu.movedown";
            this._MenuMoveDown.Text = "&Down";
            this._MenuMoveDown.Click += new System.EventHandler(this._MenuMoveDown_Click);
            // 
            // _MenuDelete
            // 
            this._MenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDelete.Image")));
            this._MenuDelete.Name = "_MenuDelete";
            this._MenuDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this._MenuDelete.Size = new System.Drawing.Size(181, 22);
            this._MenuDelete.Tag = "utstdo::menu.remove";
            this._MenuDelete.Text = "&Remove...";
            this._MenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(178, 6);
            // 
            // _MenuProperties
            // 
            this._MenuProperties.Image = ((System.Drawing.Image)(resources.GetObject("_MenuProperties.Image")));
            this._MenuProperties.Name = "_MenuProperties";
            this._MenuProperties.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this._MenuProperties.Size = new System.Drawing.Size(181, 22);
            this._MenuProperties.Tag = "utstdo::menu.properties";
            this._MenuProperties.Text = "&Properties...";
            this._MenuProperties.Click += new System.EventHandler(this._MenuProperties_Click);
            // 
            // _MenuTest
            // 
            this._MenuTest.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuRun,
            this._MenuRunStep,
            this._MenuAbort,
            this._MenuBlankTest0,
            this._MenuClear});
            this._MenuTest.Name = "_MenuTest";
            this._MenuTest.Size = new System.Drawing.Size(40, 20);
            this._MenuTest.Tag = "utstdo::menu.test";
            this._MenuTest.Text = "Te&st";
            this._MenuTest.DropDownClosed += new System.EventHandler(this._MenuTest_DropDownClosed);
            this._MenuTest.DropDownOpening += new System.EventHandler(this._MenuTest_DropDownOpening);
            // 
            // _MenuRun
            // 
            this._MenuRun.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRun.Image")));
            this._MenuRun.Name = "_MenuRun";
            this._MenuRun.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._MenuRun.Size = new System.Drawing.Size(185, 22);
            this._MenuRun.Tag = "utstdo::menu.run";
            this._MenuRun.Text = "&Run";
            this._MenuRun.Click += new System.EventHandler(this._MenuRun_Click);
            // 
            // _MenuRunStep
            // 
            this._MenuRunStep.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRunStep.Image")));
            this._MenuRunStep.Name = "_MenuRunStep";
            this._MenuRunStep.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this._MenuRunStep.Size = new System.Drawing.Size(185, 22);
            this._MenuRunStep.Tag = "utstdo::menu.runstep";
            this._MenuRunStep.Text = "Run &Step";
            this._MenuRunStep.Click += new System.EventHandler(this._MenuRunStep_Click);
            // 
            // _MenuAbort
            // 
            this._MenuAbort.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAbort.Image")));
            this._MenuAbort.Name = "_MenuAbort";
            this._MenuAbort.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.End)));
            this._MenuAbort.Size = new System.Drawing.Size(185, 22);
            this._MenuAbort.Tag = "utstdo::menu.abort";
            this._MenuAbort.Text = "&Abort";
            this._MenuAbort.Click += new System.EventHandler(this._MenuAbort_Click);
            // 
            // _MenuBlankTest0
            // 
            this._MenuBlankTest0.Name = "_MenuBlankTest0";
            this._MenuBlankTest0.Size = new System.Drawing.Size(182, 6);
            // 
            // _MenuClear
            // 
            this._MenuClear.Image = ((System.Drawing.Image)(resources.GetObject("_MenuClear.Image")));
            this._MenuClear.Name = "_MenuClear";
            this._MenuClear.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this._MenuClear.Size = new System.Drawing.Size(185, 22);
            this._MenuClear.Tag = "utstdo::menu.clear";
            this._MenuClear.Text = "&Clear Output";
            this._MenuClear.Click += new System.EventHandler(this._MenuClear_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(47, 20);
            this._MenuTools.Tag = "utstdo::menu.tools";
            this._MenuTools.Text = "&Tools";
            this._MenuTools.DropDownClosed += new System.EventHandler(this._MenuTools_DropDownClosed);
            this._MenuTools.DropDownOpening += new System.EventHandler(this._MenuTools_DropDownOpening);
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(125, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "Debug";
            this._MenuDebug.Visible = false;
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStart.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "Start &Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStop.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "Stop &Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(219, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuDebugSave.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "&Save Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this._MenuDebugView.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.U)));
            this._MenuDebugUpload.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(125, 22);
            this._MenuSettings.Tag = "utstdo::menu.settings";
            this._MenuSettings.Text = "&Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 20);
            this._MenuHelp.Tag = "utstdo::menu.help";
            this._MenuHelp.Text = "&Help";
            this._MenuHelp.DropDownClosed += new System.EventHandler(this._MenuHelp_DropDownClosed);
            this._MenuHelp.DropDownOpening += new System.EventHandler(this._MenuHelp_DropDownOpening);
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._MenuShowHelp.Size = new System.Drawing.Size(159, 22);
            this._MenuShowHelp.Tag = "utstdo::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help...";
            this._MenuShowHelp.Click += new System.EventHandler(this._MenuShowHelp_Click);
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(159, 22);
            this._MenuAbout.Tag = "utstdo::menu.about";
            this._MenuAbout.Text = "&About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _SplitMain
            // 
            this._SplitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SplitMain.Location = new System.Drawing.Point(0, 24);
            this._SplitMain.Name = "_SplitMain";
            // 
            // _SplitMain.Panel1
            // 
            this._SplitMain.Panel1.Controls.Add(this._TreeProject);
            // 
            // _SplitMain.Panel2
            // 
            this._SplitMain.Panel2.Controls.Add(this._PanelRun);
            this._SplitMain.Panel2.Controls.Add(this._PanelBrowser);
            this._SplitMain.Size = new System.Drawing.Size(1321, 746);
            this._SplitMain.SplitterDistance = 440;
            this._SplitMain.TabIndex = 1;
            this._SplitMain.Visible = false;
            // 
            // _TreeProject
            // 
            this._TreeProject.AllowDrop = true;
            this._TreeProject.ContextMenuStrip = this._CmenuTree;
            this._TreeProject.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TreeProject.ImageIndex = 0;
            this._TreeProject.ImageList = this._IlistTree;
            this._TreeProject.Location = new System.Drawing.Point(0, 0);
            this._TreeProject.Name = "_TreeProject";
            this._TreeProject.PathSeparator = "/";
            this._TreeProject.SelectedImageIndex = 0;
            this._TreeProject.Size = new System.Drawing.Size(440, 746);
            this._TreeProject.TabIndex = 0;
            this._TreeProject.DragDrop += new System.Windows.Forms.DragEventHandler(this._TreeProject_DragDrop);
            this._TreeProject.DragEnter += new System.Windows.Forms.DragEventHandler(this._TreeProject_DragEnter);
            this._TreeProject.DragOver += new System.Windows.Forms.DragEventHandler(this._TreeProject_DragOver);
            this._TreeProject.MouseDown += new System.Windows.Forms.MouseEventHandler(this._TreeProject_MouseDown);
            this._TreeProject.MouseMove += new System.Windows.Forms.MouseEventHandler(this._TreeProject_MouseMove);
            // 
            // _CmenuTree
            // 
            this._CmenuTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem,
            this._CmenuTreeBlank0,
            this._CmenuAdd,
            this._CmenuMove,
            this._CmenuDelete,
            this._CmenuTreeBlank1,
            this._CmenuProperties});
            this._CmenuTree.Name = "_CmenuTree";
            this._CmenuTree.Size = new System.Drawing.Size(137, 126);
            this._CmenuTree.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuTree_Opening);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("runToolStripMenuItem.Image")));
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.runToolStripMenuItem.Tag = "utstdo::cmenu.run";
            this.runToolStripMenuItem.Text = "Run";
            this.runToolStripMenuItem.Click += new System.EventHandler(this._MenuRunStep_Click);
            // 
            // _CmenuTreeBlank0
            // 
            this._CmenuTreeBlank0.Name = "_CmenuTreeBlank0";
            this._CmenuTreeBlank0.Size = new System.Drawing.Size(133, 6);
            // 
            // _CmenuAdd
            // 
            this._CmenuAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuAddLibrary,
            this._CmenuAddTest});
            this._CmenuAdd.Name = "_CmenuAdd";
            this._CmenuAdd.Size = new System.Drawing.Size(136, 22);
            this._CmenuAdd.Tag = "utstdo::cmenu.add";
            this._CmenuAdd.Text = "Add";
            // 
            // _CmenuAddLibrary
            // 
            this._CmenuAddLibrary.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddLibrary.Image")));
            this._CmenuAddLibrary.Name = "_CmenuAddLibrary";
            this._CmenuAddLibrary.Size = new System.Drawing.Size(119, 22);
            this._CmenuAddLibrary.Tag = "utstdo::cmenu.addlibrary";
            this._CmenuAddLibrary.Text = "Library...";
            this._CmenuAddLibrary.Click += new System.EventHandler(this._MenuAddLibrary_Click);
            // 
            // _CmenuAddTest
            // 
            this._CmenuAddTest.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddTest.Image")));
            this._CmenuAddTest.Name = "_CmenuAddTest";
            this._CmenuAddTest.Size = new System.Drawing.Size(119, 22);
            this._CmenuAddTest.Tag = "utstdo::cmenu.addtest";
            this._CmenuAddTest.Text = "Test...";
            this._CmenuAddTest.Click += new System.EventHandler(this._MenuAddTest_Click);
            // 
            // _CmenuMove
            // 
            this._CmenuMove.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuMoveUp,
            this._CmenuMoveDown});
            this._CmenuMove.Name = "_CmenuMove";
            this._CmenuMove.Size = new System.Drawing.Size(136, 22);
            this._CmenuMove.Tag = "utstdo::cmenu.move";
            this._CmenuMove.Text = "Move";
            this._CmenuMove.DropDownOpening += new System.EventHandler(this._CmenuMove_DropDownOpening);
            // 
            // _CmenuMoveUp
            // 
            this._CmenuMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuMoveUp.Image")));
            this._CmenuMoveUp.Name = "_CmenuMoveUp";
            this._CmenuMoveUp.Size = new System.Drawing.Size(105, 22);
            this._CmenuMoveUp.Tag = "utstdo::cmenu.moveup";
            this._CmenuMoveUp.Text = "Up";
            this._CmenuMoveUp.Click += new System.EventHandler(this._MenuMoveUp_Click);
            // 
            // _CmenuMoveDown
            // 
            this._CmenuMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuMoveDown.Image")));
            this._CmenuMoveDown.Name = "_CmenuMoveDown";
            this._CmenuMoveDown.Size = new System.Drawing.Size(105, 22);
            this._CmenuMoveDown.Tag = "utstdo::cmenu.movedown";
            this._CmenuMoveDown.Text = "Down";
            this._CmenuMoveDown.Click += new System.EventHandler(this._MenuMoveDown_Click);
            // 
            // _CmenuDelete
            // 
            this._CmenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDelete.Image")));
            this._CmenuDelete.Name = "_CmenuDelete";
            this._CmenuDelete.Size = new System.Drawing.Size(136, 22);
            this._CmenuDelete.Tag = "utstdo::cmenu.remove";
            this._CmenuDelete.Text = "Remove...";
            this._CmenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _CmenuTreeBlank1
            // 
            this._CmenuTreeBlank1.Name = "_CmenuTreeBlank1";
            this._CmenuTreeBlank1.Size = new System.Drawing.Size(133, 6);
            // 
            // _CmenuProperties
            // 
            this._CmenuProperties.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuProperties.Image")));
            this._CmenuProperties.Name = "_CmenuProperties";
            this._CmenuProperties.Size = new System.Drawing.Size(136, 22);
            this._CmenuProperties.Tag = "utstdo::cmenu.properties";
            this._CmenuProperties.Text = "Properties...";
            this._CmenuProperties.Click += new System.EventHandler(this._MenuProperties_Click);
            // 
            // _IlistTree
            // 
            this._IlistTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistTree.ImageStream")));
            this._IlistTree.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistTree.Images.SetKeyName(0, "studio");
            this._IlistTree.Images.SetKeyName(1, "project");
            this._IlistTree.Images.SetKeyName(2, "project_wait");
            this._IlistTree.Images.SetKeyName(3, "project_run");
            this._IlistTree.Images.SetKeyName(4, "project_ok");
            this._IlistTree.Images.SetKeyName(5, "project_err");
            this._IlistTree.Images.SetKeyName(6, "project_stop");
            this._IlistTree.Images.SetKeyName(7, "program");
            this._IlistTree.Images.SetKeyName(8, "program_wait");
            this._IlistTree.Images.SetKeyName(9, "program_run");
            this._IlistTree.Images.SetKeyName(10, "program_ok");
            this._IlistTree.Images.SetKeyName(11, "program_err");
            this._IlistTree.Images.SetKeyName(12, "program_stop");
            this._IlistTree.Images.SetKeyName(13, "dll");
            this._IlistTree.Images.SetKeyName(14, "dll_wait");
            this._IlistTree.Images.SetKeyName(15, "dll_run");
            this._IlistTree.Images.SetKeyName(16, "dll_ok");
            this._IlistTree.Images.SetKeyName(17, "dll_err");
            this._IlistTree.Images.SetKeyName(18, "dll_stop");
            this._IlistTree.Images.SetKeyName(19, "jar");
            this._IlistTree.Images.SetKeyName(20, "jar_wait");
            this._IlistTree.Images.SetKeyName(21, "jar_run");
            this._IlistTree.Images.SetKeyName(22, "jar_ok");
            this._IlistTree.Images.SetKeyName(23, "jar_err");
            this._IlistTree.Images.SetKeyName(24, "jar_stop");
            this._IlistTree.Images.SetKeyName(25, "ltest");
            this._IlistTree.Images.SetKeyName(26, "ltest_wait");
            this._IlistTree.Images.SetKeyName(27, "ltest_run");
            this._IlistTree.Images.SetKeyName(28, "ltest_ok");
            this._IlistTree.Images.SetKeyName(29, "ltest_err");
            this._IlistTree.Images.SetKeyName(30, "ltest_stop");
            this._IlistTree.Images.SetKeyName(31, "test");
            this._IlistTree.Images.SetKeyName(32, "test_wait");
            this._IlistTree.Images.SetKeyName(33, "test_run");
            this._IlistTree.Images.SetKeyName(34, "test_ok");
            this._IlistTree.Images.SetKeyName(35, "test_err");
            this._IlistTree.Images.SetKeyName(36, "test_stop");
            this._IlistTree.Images.SetKeyName(37, "gear_0");
            this._IlistTree.Images.SetKeyName(38, "gear_1");
            // 
            // _PanelRun
            // 
            this._PanelRun.Controls.Add(this._LabelRunText);
            this._PanelRun.Controls.Add(this._LabelRun);
            this._PanelRun.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._PanelRun.Location = new System.Drawing.Point(0, 710);
            this._PanelRun.Name = "_PanelRun";
            this._PanelRun.Size = new System.Drawing.Size(877, 36);
            this._PanelRun.TabIndex = 2;
            this._PanelRun.Visible = false;
            // 
            // _LabelRunText
            // 
            this._LabelRunText.AutoSize = true;
            this._LabelRunText.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelRunText.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelRunText.Location = new System.Drawing.Point(60, 12);
            this._LabelRunText.Name = "_LabelRunText";
            this._LabelRunText.Size = new System.Drawing.Size(151, 13);
            this._LabelRunText.TabIndex = 1;
            this._LabelRunText.Text = "Running tests. Please wait...";
            // 
            // _LabelRun
            // 
            this._LabelRun.Image = ((System.Drawing.Image)(resources.GetObject("_LabelRun.Image")));
            this._LabelRun.Location = new System.Drawing.Point(9, 6);
            this._LabelRun.Name = "_LabelRun";
            this._LabelRun.Size = new System.Drawing.Size(24, 24);
            this._LabelRun.TabIndex = 0;
            // 
            // _PanelBrowser
            // 
            this._PanelBrowser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelBrowser.Controls.Add(this._BrowserOut);
            this._PanelBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PanelBrowser.Location = new System.Drawing.Point(0, 0);
            this._PanelBrowser.Name = "_PanelBrowser";
            this._PanelBrowser.Size = new System.Drawing.Size(877, 746);
            this._PanelBrowser.TabIndex = 1;
            // 
            // _BrowserOut
            // 
            this._BrowserOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this._BrowserOut.Location = new System.Drawing.Point(0, 0);
            this._BrowserOut.MinimumSize = new System.Drawing.Size(20, 20);
            this._BrowserOut.Name = "_BrowserOut";
            this._BrowserOut.Size = new System.Drawing.Size(875, 744);
            this._BrowserOut.TabIndex = 0;
            // 
            // _TimerRun
            // 
            this._TimerRun.Interval = 320;
            this._TimerRun.Tick += new System.EventHandler(this._TimerRun_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1321, 770);
            this.Controls.Add(this._SplitMain);
            this.Controls.Add(this._MenuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMain";
            this.Text = "Unit Test Studio";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this._MenuMain.ResumeLayout(false);
            this._MenuMain.PerformLayout();
            this._SplitMain.Panel1.ResumeLayout(false);
            this._SplitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._SplitMain)).EndInit();
            this._SplitMain.ResumeLayout(false);
            this._CmenuTree.ResumeLayout(false);
            this._PanelRun.ResumeLayout(false);
            this._PanelRun.PerformLayout();
            this._PanelBrowser.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _MenuMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpen;
        private System.Windows.Forms.ToolStripMenuItem _MenuClose;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank2;
        private System.Windows.Forms.ToolStripMenuItem _MenuSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank0;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank3;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuTest;
        private System.Windows.Forms.ToolStripMenuItem _MenuRunStep;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.ToolStripMenuItem _MenuNew;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuAdd;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddLibrary;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddTest;
        private System.Windows.Forms.SplitContainer _SplitMain;
        private System.Windows.Forms.TreeView _TreeProject;
        private System.Windows.Forms.WebBrowser _BrowserOut;
        private System.Windows.Forms.Panel _PanelBrowser;
        private System.Windows.Forms.ToolStripSeparator _MenuBlankTest0;
        private System.Windows.Forms.ToolStripMenuItem _MenuClear;
        private System.Windows.Forms.ImageList _IlistTree;
        private System.Windows.Forms.ToolStripMenuItem _MenuExport;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _MenuMove;
        private System.Windows.Forms.ToolStripMenuItem _MenuMoveUp;
        private System.Windows.Forms.ToolStripMenuItem _MenuMoveDown;
        private System.Windows.Forms.ToolStripMenuItem _MenuDelete;
        private System.Windows.Forms.ToolStripMenuItem _MenuRun;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem _MenuProperties;
        private System.Windows.Forms.ContextMenuStrip _CmenuTree;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _CmenuTreeBlank0;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAdd;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddLibrary;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddTest;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDelete;
        private System.Windows.Forms.ToolStripMenuItem _CmenuProperties;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMove;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMoveUp;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMoveDown;
        private System.Windows.Forms.ToolStripSeparator _CmenuTreeBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbort;
        private System.Windows.Forms.Timer _TimerRun;
        private System.Windows.Forms.Panel _PanelRun;
        private System.Windows.Forms.Label _LabelRunText;
        private System.Windows.Forms.Label _LabelRun;
        private System.Windows.Forms.ToolStripMenuItem _MenuRecent;
    }
}