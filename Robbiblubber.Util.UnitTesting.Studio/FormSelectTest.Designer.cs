﻿namespace Robbiblubber.Util.UnitTesting.Studio
{
    partial class FormSelectTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelectTest));
            this._ListTest = new System.Windows.Forms.ListView();
            this._ColName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._IlistTest = new System.Windows.Forms.ImageList(this.components);
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _ListTest
            // 
            this._ListTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ListTest.CheckBoxes = true;
            this._ListTest.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._ColName});
            this._ListTest.FullRowSelect = true;
            this._ListTest.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._ListTest.Location = new System.Drawing.Point(12, 12);
            this._ListTest.Name = "_ListTest";
            this._ListTest.Size = new System.Drawing.Size(686, 347);
            this._ListTest.SmallImageList = this._IlistTest;
            this._ListTest.TabIndex = 0;
            this._ListTest.UseCompatibleStateImageBehavior = false;
            this._ListTest.View = System.Windows.Forms.View.Details;
            this._ListTest.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this._ListTest_ItemChecked);
            // 
            // _ColName
            // 
            this._ColName.Text = "Name";
            this._ColName.Width = 660;
            // 
            // _IlistTest
            // 
            this._IlistTest.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistTest.ImageStream")));
            this._IlistTest.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistTest.Images.SetKeyName(0, "test");
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(431, 377);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Tag = "utstdo::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(562, 377);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(125, 28);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Tag = "utstdo::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // FormSelectTest
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(710, 425);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ListTest);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSelectTest";
            this.Tag = "utstdo::udiag.selecttests.caption";
            this.Text = "Select Tests";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView _ListTest;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.ColumnHeader _ColName;
        private System.Windows.Forms.ImageList _IlistTest;
        private System.Windows.Forms.ToolTip _ToolTip;
    }
}