﻿using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Util.UnitTesting;



namespace Robbiblubber.Util.UnitTesting.Studio
{
    /// <summary>This class implements the settings window.</summary>
    public partial class FormSettings: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormSettings()
        {
            InitializeComponent();

            if(Locale.Locales.Count == 0)
            {
                _LabelLanguage.Visible = _LcboLanguage.Visible = false;
            }

            this.Localize();

            Icon = Icon.FromHandle(((Bitmap) _IlistSettings.Images["settings"]).GetHicon());

            _CboLevel.Items.Add(new RichComboItem("utstdo::udiag.settings.minimum".Localize("Minimum"), "min", ReportingLevel.MINIMUM));
            _CboLevel.Items.Add(new RichComboItem("utstdo::udiag.settings.standard".Localize("Standard"), "default", ReportingLevel.STANDARD));
            _CboLevel.Items.Add(new RichComboItem("utstdo::udiag.settings.detailed".Localize("Detailed"), "all", ReportingLevel.DETAILED));

            switch(UnitTest.Level)
            {
                case ReportingLevel.MINIMUM:
                    _CboLevel.SelectedIndex = 0; break;
                case ReportingLevel.DETAILED:
                    _CboLevel.SelectedIndex = 2; break;
                default:
                    _CboLevel.SelectedIndex = 1; break;
            }

            _ChkDebug.Checked = Program.__Main.__ShowDebug;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, System.EventArgs e)
        {
            UnitTest.Level = ((ReportingLevel) ((RichComboItem) _CboLevel.SelectedItem).Tag);
            Program.__Main.__ShowDebug = _ChkDebug.Checked;

            if(_LcboLanguage.Value != Locale.Selected)
            {
                Locale.Selected = _LcboLanguage.Value;
                Locale.SaveSelection("Robbiblubber.Util.NET");

                Program.__Main.Localize();
            }

            Program.__Main.__SaveSettings();
        }
    }
}
