﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This attribute marks a unit test class as allowing parallel execution.</summary>
    public class ParallelAttribute : Attribute
    {}
}
