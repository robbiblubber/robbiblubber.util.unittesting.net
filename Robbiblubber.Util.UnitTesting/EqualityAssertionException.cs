﻿using System;



namespace Robbiblubber.Util.UnitTesting.Exceptions
{
    /// <summary>This class implements an assertion exception.</summary>
    public class EqualityAssertionException: AssertionException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner Exception.</param>
        public EqualityAssertionException(string message, Exception innerException = null): base(message, innerException)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="expected">Expected value.</param>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner Exception.</param>
        public EqualityAssertionException(object expected, object obtained, string message, Exception innerException = null) : base(message, innerException)
        {
            Expected = expected;
            Obtained = obtained;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="innerException">Inner Exception.</param>
        public EqualityAssertionException(Exception innerException): base(innerException)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="expected">Expected value.</param>
        /// <param name="obtained">Obtained value.</param>
        public EqualityAssertionException(object expected, object obtained) : this(expected, obtained, __GetMessage(expected, obtained))
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses an object to a string representation for output.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>String.</returns>
        internal static string __ToString(object obj)
        {
            if(obj == null)
            {
                return "NULL";
            }
            else if(obj is string)
            {
                return ("\"" + ((string) obj) + "\"");
            }
            else if(obj.GetType().IsValueType)
            {
                return obj.ToString();
            }
            else return ("[" + obj.ToString() + "]");
        }


        /// <summary>Gets a message.</summary>
        /// <param name="expected">Expected value.</param>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="details">Details.</param>
        internal static string __GetMessage(object expected, object obtained, string details = null)
        {
            return ("Assertion failed. Expected: " + __ToString(expected) + "; Obtained: " + __ToString(obtained) + "." + (string.IsNullOrWhiteSpace(details) ? "" : (" " + details.Trim())));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the expected value for the assertion.</summary>
        public object Expected
        {
            get; protected set;
        }


        /// <summary>Gets the obtained value for the assertion.</summary>
        public object Obtained
        {
            get; protected set;
        }
    }
}
