﻿using System;



namespace Robbiblubber.Util.UnitTesting.Exceptions
{
    /// <summary>This class implements an assertion exception.</summary>
    public class IdentityAssertionException: AssertionException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner Exception.</param>
        public IdentityAssertionException(string message, Exception innerException = null) : base(message, innerException)
        {}
    }
}
