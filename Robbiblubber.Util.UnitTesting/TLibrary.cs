﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Robbiblubber.Util.Debug;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a library.</summary>
    public class TLibrary: UnitTest, IUnitTest, IEnumerable<IUnitTest>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tests.</summary>
        protected List<IUnitTest> _Items = new List<IUnitTest>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected TLibrary()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="file">File name.</param>
        public TLibrary(string file)
        {
            _FileName = Path.GetFullPath(file);

            Assembly a;
            try
            {
                a = Assembly.LoadFile(file);
            }
            catch(Exception)
            {
                Version = new Version(0, 0);
                _Name = AssemblyName = "<not an assembly>";
                
                return;
            }

            try
            {
                Version = a.GetName().Version;
                _Name = AssemblyName = a.GetName().Name;
            }
            catch(Exception ex) { DebugOp.Dump("UTSDOX00102", ex); }

            try
            {
                foreach(IUnitTest i in UnitTest.GetTests(a))
                {
                    _Items.Add(i);
                }
            }
            catch(Exception ex) { DebugOp.Dump("UTSDOX00101", ex); }

            _Items.Sort();
        }




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads a library.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Libarary.</returns>
        public static TLibrary Load(string fileName)
        {
            if(fileName.ToLower().EndsWith(".jar"))
            {
                return new JarLibrary(fileName);
            }
            else
            {
                return new TLibrary(fileName);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the library file version.</summary>
        public Version Version
        {
            get; protected set;
        }


        /// <summary>Gets the library assembly name.</summary>
        public string AssemblyName
        {
            get; protected set;
        }


        /// <summary>Gets the number of tests in this library.</summary>
        public int Count
        {
            get { return _Items.Count; }
        }


        /// <summary>Gets a unit test by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Unit test.</returns>
        public IUnitTest this[int i]
        {
            get { return _Items[i]; }
        }


        /// <summary>Gets a unit test by its name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Unit test.</returns>
        public IUnitTest this[string name]
        {
            get
            {
                foreach(IUnitTest i in _Items)
                {
                    if(i.Name == name) { return i; }
                }

                return null;
            }
        }


        /// <summary>Gets a library type string.</summary>
        public string LibraryTypeString
        {
            get
            {
                if(FileName.ToLower().EndsWith(".exe")) { return "program"; }
                if(FileName.ToLower().EndsWith(".jar")) { return "jar"; }

                return "dll";
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sorts the items in this library.</summary>
        public void Sort()
        {
            _Items.Sort();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] UnitTest                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Runs a test.</summary>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was completed successful, otherwise returns FALSE.</returns>
        public override bool Run(ITestHost host)
        {
            bool rval = true;

            foreach(IUnitTest i in this)
            {
                try
                {
                    i.Prepare(host);
                    rval = (rval && i.Run(host));
                    i.Cleanup(host);
                }
                catch(Exception) { rval = false; }
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<IUntitTest>                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<IUnitTest> IEnumerable<IUnitTest>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }
    }
}
