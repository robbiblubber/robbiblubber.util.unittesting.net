﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>Test hosts implement this interface for writing test stages.</summary>
    public interface ITestHostWriter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a test sequence header.</summary>
        /// <param name="caption">Caption.</param>
        /// <param name="text">Text.</param>
        void WriteHeader(string caption, string text);


        /// <summary>Writes a unit test header.</summary>
        /// <param name="test">Unit test.</param>
        void WriteTestHeader(IUnitTest test);


        /// <summary>Writes a test result.</summary>
        /// <param name="test">Unit test.</param>
        /// <param name="result">Result.</param>
        void WriteTestResult(IUnitTest test, bool result);


        /// <summary>Writes a test sequence result.</summary>
        /// <param name="result">Result.</param>
        /// <param name="text">Text.</param>
        void WriteResult(bool result, string text = null);
    }
}
