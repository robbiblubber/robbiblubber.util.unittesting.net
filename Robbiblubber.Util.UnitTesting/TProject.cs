﻿using System;
using System.Collections.Generic;
using System.IO;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a Test Studio project.</summary>
    public class TProject
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        private List<IUnitTest> _Items = new List<IUnitTest>();

        /// <summary>Initial source text.</summary>
        private string _InitialSource = "";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TProject()
        {
            Name = "Unnamed";
            FileName = null;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="file">File name.</param>
        public TProject(string file)
        {
            Ddp cfg = Ddp.Load(file);
            
            Name = cfg.GetString("/uttxp/name");
            AllowParallel = cfg.GetBoolean("/uttxp/parallel", true);
            FileName = Path.GetFullPath(file);

            _InitialSource = cfg.ToString();

            foreach(DdpSection i in cfg.GetSection("/libraries").Sections)
            {
                TLibrary lib = TLibrary.Load(i.GetString("file"));
                lib.SortIndex = i.GetInteger("sort");

                foreach(DdpEntry j in i.GetSection("items").Entries)
                {
                    try
                    {
                        lib[j.Name].SortIndex = j.IntegerValue;
                    }
                    catch(Exception) {}
                }

                lib.Sort();
                _Items.Add(lib);
            }

            foreach(DdpSection i in cfg.GetSection("/tests").Sections)
            {
                try
                {
                    IUnitTest m = UnitTest.CreateInstance(i.GetString("file"), i.GetString("class"));

                    if(m != null)
                    {
                        m.SortIndex = i.GetInteger("sort");
                        _Items.Add(m);
                    }
                }
                catch(Exception ex) { DebugOp.Dump("UTSDOX00024", ex); }
            }

            _Items.Sort();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the current project.</summary>
        public static TProject Current
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads a project.</summary>
        /// <param name="file">File name.</param>
        /// <returns>Project object.</returns>
        public static TProject Load(string file)
        {
            return new TProject(file);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the project.</summary>
        /// <param name="file">File name.</param>
        public void Save(string file = null)
        {
            if(file != null) { FileName = file; }
            
            if(Name == "Unnamed")
            {
                Name = Path.GetFileName(FileName);
                if(Name.Contains(".")) { Name = Name.Substring(0, Name.LastIndexOf('.')); }
            }

            Ddp cfg = _GetConfig();

            cfg.Save(FileName);
            _InitialSource = cfg.ToString();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the project name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the project file name.</summary>
        public string FileName
        {
            get; set;
        }


        /// <summary>Gets the libraries in this project.</summary>
        public List<IUnitTest> Items
        {
            get { return _Items; }
        }

                
        /// <summary>Gets the project source text.</summary>
        public string Source
        {
            get { return _GetConfig().ToString(); }
        }


        /// <summary>Gets if the object is unchanged.</summary>
        public bool Virgin
        {
            get { return (Source == _InitialSource); }
        }


        /// <summary>Gets or sets if parallel execution is allowed in this project.</summary>
        public bool AllowParallel
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a configuration file for this instance.</summary>
        /// <returns>Configuration file.</returns>
        private Ddp _GetConfig()
        {
            Ddp rval = new Ddp();

            DdpSection sec = rval.Sections.Add("uttxp");
            sec.SetValue("version", "1.0");
            sec.SetValue("name", Name);
            sec.SetValue("parallel", AllowParallel);

            int nl = 0;
            int nt = 0;
            foreach(IUnitTest i in _Items)
            {
                if(i is TLibrary)
                {
                    rval.SetValue("/libraries/" + i.Name + "[" + nl.ToString() + "]/file", i.FileName);
                    rval.SetValue("/libraries/" + i.Name + "[" + nl.ToString() + "]/sort", i.SortIndex);

                    sec = rval.GetSection("/libraries/" + i.Name + "[" + nl.ToString() + "]/items");
                    foreach(IUnitTest j in ((TLibrary) i))
                    {
                        sec.SetValue(j.Name, j.SortIndex);
                    }

                    nl++;
                }
                else
                {
                    rval.SetValue("/tests/" + i.Name + "[" + nt.ToString() + "]/file", i.FileName);
                    rval.SetValue("/tests/" + i.Name + "[" + nt.ToString() + "]/class", i.ClassName);
                    rval.SetValue("/tests/" + i.Name + "[" + nt.ToString() + "]/sort", i.SortIndex);

                    nt++;
                }
            }

            return rval;
        }
    }
}
