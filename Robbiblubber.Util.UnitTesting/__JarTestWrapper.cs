﻿using System;
using System.IO;
using System.Threading;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class provides a wrapper for java tests.</summary>
    internal sealed class __JarTestWrapper: IUnitTest
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="ddp">Ddp string.</param>
        /// <param name="fileName">File name.</param>
        /// <param name="n">Index.</param>
        internal __JarTestWrapper(Ddp ddp, string fileName, int n)
        {
            FileName = fileName;

            Name = ddp.GetString("name[" + n.ToString() + "]");
            Description = ddp.GetString("description[" + n.ToString() + "]");
            ClassName = ddp.GetString("class[" + n.ToString() + "]");
            SortIndex = ddp.GetInteger("sort[" + n.ToString() + "]");
            SortIndex = ddp.GetInteger("parallelizable[" + n.ToString() + "]");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IUnitTest                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the test name.</summary>
        public string Name
        {
            get; private set;
        }


        /// <summary>Gets the test description.</summary>
        public string Description
        {
            get; private set;
        }


        /// <summary>Gets the file name.</summary>
        public string FileName
        {
            get; private set;
        }


        /// <summary>Gets the class name.</summary>
        public string ClassName
        {
            get; private set;
        }


        /// <summary>Gets the sort index.</summary>
        public int SortIndex
        {
            get; set;
        }


        /// <summary>Gets if the unit test can be run in parallel independent from other unit tests.</summary>
        public bool Parallelizable
        {
            get; private set;
        }


        /// <summary>Gets or sets the unit test data.</summary>
        public object Data
        {
            get; set;
        }


        /// <summary>Prepares the test environment.</summary>
        /// <param name="host">Host.</param>
        public void Prepare(ITestHost host)
        {}


        /// <summary>Cleans up the test environment.</summary>
        /// <param name="host">Host.</param>
        public void Cleanup(ITestHost host)
        {}


        /// <summary>Runs a test.</summary>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was completed successful, otherwise returns FALSE.</returns>
        public bool Run(ITestHost host)
        {
            string temp = FileOp.CreateTempPath();
            string ctrl = (temp + @"\control.ddp");
            string rslt = (temp + @"\result.ddp");

            Ddp ddp = new DdpString();

            ddp.SetValue("op", "cinvoke");
            ddp.SetValue("file", FileName);
            ddp.SetValue("class", ClassName);
            ddp.Save(ctrl);

            JarLibrary.__JavaInvoke(temp);

            while(File.Exists(ctrl)) { Thread.Sleep(60); }

            ddp = Ddp.Load(rslt);

            int count = ddp.GetInteger("count");

            for(int i = 0; i < count; i++)
            {
                switch(ddp.GetString("method[" + i.ToString() + "]").ToLower())
                {
                    case "write":
                        host.Write(ddp.GetString("text[" + i.ToString() + "]"), (MessageType) ddp.GetInteger("type[" + i.ToString() + "]")); break;
                    case "writeheader":
                        ((ITestHostWriter) host).WriteHeader(ddp.GetString("caption[" + i.ToString() + "]"), ddp.GetString("text[" + i.ToString() + "]")); break;
                    case "writeresult":
                        ((ITestHostWriter) host).WriteResult(ddp.GetBoolean("result[" + i.ToString() + "]"), ddp.GetString("text[" + i.ToString() + "]")); break;
                    case "writetestheader":
                        ((ITestHostWriter) host).WriteTestHeader(this); break;
                    case "writetestresult":
                        ((ITestHostWriter) host).WriteTestResult(this, ddp.GetBoolean("result[" + i.ToString() + "]")); break;
                }
            }

            Directory.Delete(temp, true);

            return ddp.GetBoolean("result");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares this instance to an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Comparison result.</returns>
        int IComparable.CompareTo(object obj)
        {
            if(obj is IUnitTest)
            {
                if(((IUnitTest) obj).SortIndex == SortIndex)
                {
                    return Name.CompareTo(((IUnitTest) obj).Name);
                }
                return SortIndex.CompareTo(((IUnitTest) obj).SortIndex);
            }

            return -1;
        }
    }
}
