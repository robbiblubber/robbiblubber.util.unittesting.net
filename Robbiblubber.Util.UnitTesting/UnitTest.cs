﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;

using Robbiblubber.Util.UnitTesting.Exceptions;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements the unit test interface and provides methods for unit test manipulation.</summary>
    public abstract class UnitTest: IUnitTest, IComparable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static members                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reporting level.</summary>
        protected static ReportingLevel _Level = ReportingLevel.STANDARD;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Test name.</summary>
        protected string _Name;

        /// <summary>File name.</summary>
        protected string _FileName;

        /// <summary>Class name.</summary>
        protected string _ClassName;

        /// <summary>Test description.</summary>
        protected string _Description = "";

        /// <summary>Parallelizable flag.</summary>
        protected bool _Parallelizable = true;
        
        /// <summary>Sort index.</summary>
        protected int _SortIndex = 1024;

        /// <summary>Data.</summary>
        protected object _Data = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public UnitTest()
        {
            _Name = GetType().Name;
            UriBuilder b = new UriBuilder(GetType().Assembly.CodeBase);
            _FileName = Path.GetFullPath(Uri.UnescapeDataString(b.Path));
            _ClassName = GetType().FullName;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the reporting level.</summary>
        public static ReportingLevel Level
        {
            get { return _Level; }
            set { _Level = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Runs all tests in an assembly.</summary>
        /// <param name="host">Test host.</param>
        /// <param name="assembly">Assembly.</param>
        /// <returns>Returns TRUE if all tests were successful, otherwise returns FALSE.</returns>
        public static bool RunAssembly(ITestHost host = null, Assembly assembly = null)
        {
            if(host == null) { host = new ConsoleTestHost(); }
            if(assembly == null) { assembly = Assembly.GetEntryAssembly(); }

            if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteHeader("Running test in " + assembly.GetName().Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); }
            bool result = true;

            foreach(IUnitTest i in GetTests(assembly))
            {
                if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteTestHeader(i); }
                bool r = i.Run(host);
                if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteTestResult((IUnitTest) i, r); }

                result = (result && r);
            }

            if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteResult(result); }

            return result;
        }


        /// <summary>Runs all tests in an assembly.</summary>
        /// <param name="assembly">Assembly.</param>
        public static void RunAssembly(Assembly assembly)
        {
            RunAssembly(null, assembly);
        }


        /// <summary>Runs all tests in an instance.</summary>
        /// <param name="cls">Class type.</param>
        /// <param name="instance">Instance.</param>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was successful, otherwise returns FALSE.</returns>
        public static bool RunInstance(Type cls, object instance, ITestHost host)
        {
            bool rval = true;

            foreach(MethodInfo i in GetMethods(cls, typeof(TestAttribute)))
            {
                rval = (rval && _Invoke(instance, i, host));
            }

            return rval;
        }


        /// <summary>Runs all tests in an instance.</summary>
        /// <param name="instance">Instance.</param>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was successful, otherwise returns FALSE.</returns>
        public static bool RunInstance(object instance, ITestHost host)
        {
            return RunInstance(instance.GetType(), instance, host);
        }



        /// <summary>Runs all tests in an instance.</summary>
        /// <param name="method">Method.</param>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was successful, otherwise returns FALSE.</returns>
        public static bool RunMethod(Action method, ITestHost host)
        {
            return _Invoke(null, method, host);
        }


        /// <summary>Runs a test.</summary>
        /// <param name="host">Host.</param>
        /// <param name="target">Target object.</param>
        /// <param name="recursive">Determines if the call is recursive.</param>
        public static bool Run(ITestHost host, object target, bool recursive = false)
        {
            if(!recursive)
            {
                if(host is HTMLOutputTestHost) { ((HTMLOutputTestHost) host).Clear(); }
            }

            bool rval = true;

            if(target is TProject)
            {
                if(!recursive)
                {
                    if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteHeader(((TProject) target).Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); }
                }

                foreach(IUnitTest i in TProject.Current.Items)
                {
                    rval = (rval && Run(host, i, true));
                }
            }
            else if(target is TLibrary)
            {
                if(!recursive)
                {
                    if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteHeader(((TLibrary) target).Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); }
                }

                foreach(IUnitTest i in ((TLibrary) target))
                {
                    rval = (rval && Run(host, i, true));
                }
            }
            else
            {
                if(!recursive)
                {
                    if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteHeader(((IUnitTest) target).Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")); }
                }

                if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteTestHeader((IUnitTest) target); }
                ((IUnitTest) target).Prepare(host);

                rval = ((IUnitTest) target).Run(host);
                ((IUnitTest) target).Cleanup(host);

                if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteTestResult((IUnitTest) target, rval); }
            }

            if(!recursive)
            {
                if(host is ITestHostWriter) { ((ITestHostWriter) host).WriteResult(rval); }
            }

            return rval;
        }


        /// <summary>Gets all tests in an assembly.</summary>
        /// <param name="assembly">Assembly.</param>
        /// <returns>Array of tests.</returns>
        public static IUnitTest[] GetTests(Assembly assembly = null)
        {
            List<IUnitTest> rval = new List<IUnitTest>();

            rval.AddRange(GetTestsByInterface(assembly));
            rval.AddRange(GetTestsByAttribute(assembly));

            return rval.ToArray();
        }


        /// <summary>Gets all tests that implement IUnitTest in an assembly.</summary>
        /// <param name="assembly">Assembly.</param>
        /// <returns>Array of tests.</returns>
        public static IUnitTest[] GetTestsByInterface(Assembly assembly = null)
        {
            if(assembly == null) { assembly = Assembly.GetEntryAssembly(); }

            List<IUnitTest> rval = new List<IUnitTest>();

            foreach(Type i in assembly.GetTypes())
            {
                if(i.IsInterface || i.IsAbstract) continue;

                if(i.GetInterfaces().Contains(typeof(IUnitTest)))
                {
                    try
                    {
                        IUnitTest m = (IUnitTest) Activator.CreateInstance(i);
                        rval.Add(m);
                    }
                    catch(Exception) {}
                }
            }

            return rval.ToArray();
        }


        /// <summary>Gets all tests that are attributed [Test] in an assembly.</summary>
        /// <param name="assembly">Assembly.</param>
        /// <returns>Array of tests.</returns>
        public static IUnitTest[] GetTestsByAttribute(Assembly assembly = null)
        {
            if(assembly == null) { assembly = Assembly.GetEntryAssembly(); }

            List<IUnitTest> rval = new List<IUnitTest>();

            foreach(Type i in assembly.GetTypes())
            {
                if(GetAttribute(i, typeof(TestAttribute)) != null)
                {
                    try
                    {
                        rval.Add(new __UnitTestWrapper(i));
                    }
                    catch(Exception) {}
                }
            }

            return rval.ToArray();
        }


        /// <summary>Returns the test attribute for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="attribute">Attribute type.</param>
        /// <returns>Attribute.</returns>
        public static Attribute GetAttribute(object obj, Type attribute)
        {
            if(obj is MethodInfo)
            {
                return _GetAttribute((MethodInfo) obj, attribute);
            }
            else if(obj is Action)
            {
                return _GetAttribute(((Action) obj).Method, attribute);
            }
            else if(obj is Type)
            {
                foreach(object i in ((Type) obj).GetCustomAttributes(attribute, true))
                {
                    return ((Attribute) i);
                }
            }

            return null;
        }


        /// <summary>Gets a set of methods for an attribute.</summary>
        /// <param name="cls">Class type.</param>
        /// <param name="attribute">Attribute type.</param>
        /// <returns>Array of methods.</returns>
        public static MethodInfo[] GetMethods(Type cls, Type attribute)
        {
            List<MethodOrderAttribute> list = new List<MethodOrderAttribute>();

            foreach(MethodInfo i in cls.GetMethods())
            {
                if(_GetAttribute(i, attribute) != null)
                {
                    MethodOrderAttribute m = (MethodOrderAttribute) _GetAttribute(i, typeof(MethodOrderAttribute));
                    if(m == null) { m = new MethodOrderAttribute(); }

                    m.__Method = i;

                    list.Add(m);
                }
            }

            list.Sort();

            MethodInfo[] rval = new MethodInfo[list.Count];

            for(int i = 0; i < list.Count; i++)
            {
                rval[i] = list[i].__Method;
            }

            return rval;
        }


        /// <summary>Loads a test object.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="className">Class name.</param>
        /// <returns>Test.</returns>
        public static IUnitTest CreateInstance(string fileName, string className)
        {
            if(fileName.ToLower().EndsWith(".jar"))
            {
                string temp = FileOp.CreateTempPath();
                string ctrl = (temp + @"\control.ddp");
                string rslt = (temp + @"\result.ddp");

                Ddp ddp = new DdpString();

                ddp.SetValue("op", "cload");
                ddp.SetValue("file", fileName);
                ddp.SetValue("class", className);
                ddp.Save(ctrl);

                JarLibrary.__JavaInvoke(temp);

                while(File.Exists(ctrl)) { Thread.Sleep(60); }

                ddp = Ddp.Load(rslt);
                Thread.Sleep(20);
                Directory.Delete(temp, true);

                return new __JarTestWrapper(ddp, fileName, 0);
            }
            else
            {
                Assembly a = Assembly.LoadFile(fileName);

                Type n = a.GetType(className);
                if(GetAttribute(a.GetType(className), typeof(TestAttribute)) == null)
                {
                    return ((IUnitTest) Activator.CreateInstance(a.GetType(className)));
                }
                else
                {
                    return new __UnitTestWrapper(a.GetType(className));
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the Test attribute from a method.</summary>
        /// <param name="method">Method.</param>
        /// <param name="attribute">Attribute type.</param>
        /// <returns>Test attribute.</returns>
        protected static Attribute _GetAttribute(MethodInfo method, Type attribute)
        {
            foreach(object i in method.GetCustomAttributes(attribute, true))
            {
                return ((Attribute) i);
            }

            return null;
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="instance">Instance.</param>
        /// <param name="method">Method.</param>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was successful, otherwise returns FALSE.</returns>
        protected static bool _Invoke(object instance, object method, ITestHost host)
        {
            TestAttribute attr = (TestAttribute) GetAttribute(method, typeof(TestAttribute));
            string name = "";

            if(method is Action)
            {
                name = ((Action) method).Method.Name;
            }
            else if(method is MethodInfo) { name = ((MethodInfo) method).Name; }
            string desc = null;
            
            if(attr != null)
            {
                if(!string.IsNullOrWhiteSpace(attr.Name)) { name = attr.Name; }
                desc = attr.Description;
            }

            if((int) _Level >= (int) ReportingLevel.DETAILED)
            {
                host.Write(name + (string.IsNullOrWhiteSpace(desc) ? "" : ": " + desc), MessageType.INFORMATION);
                host.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            
            try
            {
                if(method is Action)
                {
                    ((Action) method)();
                }
                else if(method is MethodInfo)
                {
                    if(((MethodInfo) method).GetParameters().Length == 1)
                    {
                        ((MethodInfo) method).Invoke(instance, new object[] { host });
                    }
                    else
                    {
                        ((MethodInfo) method).Invoke(instance, new object[0]);
                    }
                }
            }
            catch(Exception ex)
            {
                host.Write(_GetCauseException(ex).Message, MessageType.WARNING);

                if((int) _Level >= (int) ReportingLevel.DETAILED)
                {
                    host.Write(ex.Message, MessageType.INFORMATION);
                    host.Write(ex.StackTrace, MessageType.INFORMATION);
                }
                host.Write(name + " has failed.", MessageType.FAILURE);

                return false;
            }

            host.Write(name + " has been tested successfully.", MessageType.SUCCESS);
            return true;
        }


        /// <summary>Returns the assertion exception that caused the failure.</summary>
        /// <param name="ex">Exception.</param>
        /// <returns>Exception.</returns>
        private static Exception _GetCauseException(Exception ex)
        {
            while(!(ex is AssertionException))
            {
                if(ex.InnerException == null) break;
                ex = ex.InnerException;
            }

            return ex;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IUnitTest                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Prepares the test environment.</summary>
        /// <param name="host">Host.</param>
        public virtual void Prepare(ITestHost host)
        {}


        /// <summary>Cleans up the test environment.</summary>
        /// <param name="host">Host.</param>
        public virtual void Cleanup(ITestHost host)
        {}


        /// <summary>Runs a test.</summary>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was completed successful, otherwise returns FALSE.</returns>
        public abstract bool Run(ITestHost host);


        /// <summary>Gets the test name.</summary>
        public virtual string Name
        {
            get { return _Name; }
            protected set { _Name = value; }
        }


        /// <summary>Gets the test description.</summary>
        public virtual string Description
        {
            get { return _Description; }
            protected set { _Description = value; }
        }


        /// <summary>Gets the library file name.</summary>
        public virtual string FileName
        {
            get { return _FileName; }
            protected set { _FileName = value; }
        }


        /// <summary>Gets the class name.</summary>
        public virtual string ClassName
        {
            get { return _ClassName; }
            protected set { _ClassName = value; }
        }


        /// <summary>Gets if the unit test can be run in parallel independent from other unit tests.</summary>
        public virtual bool Parallelizable
        {
            get { return _Parallelizable; }
            protected set { _Parallelizable = value; }
        }


        /// <summary>Gets or sets the sort index.</summary>
        public virtual int SortIndex
        {
            get { return _SortIndex; }
            set { _SortIndex = value; }
        }


        /// <summary>Gets or sets the unit test data.</summary>
        public virtual object Data
        {
            get { return _Data; }
            set { _Data = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares this instance to an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Comparison result.</returns>
        int IComparable.CompareTo(object obj)
        {
            if(obj is IUnitTest)
            {
                if(((IUnitTest) obj).SortIndex == _SortIndex)
                {
                    return Name.CompareTo(((IUnitTest) obj).Name);
                }
                return _SortIndex.CompareTo(((IUnitTest) obj).SortIndex);
            }

            return -1;
        }
    }
}
