﻿using System;
using System.Text;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a HTML output test host.</summary>
    public class HTMLOutputTestHost: ITestHost, ITestHostWriter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Lead string.</summary>
        private string _LEAD = "<!DOCTYPE html>\n<html>\n<head>\n<title>Output</title>\n\n<link rel=\"stylesheet\" type=\"text/css\" href=\"%CSS%\" />\n</head>\n<body>";

        /// <summary>Tail string.</summary>
        private const string _TAIL = "</body>\n</html>\n";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Output body.</summary>
        private StringBuilder _Body;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public HTMLOutputTestHost()
        {
            NullIcon    = "http://robbiblubber.lima-city.de/utsto/res/null.png";
            InfoIcon    = "http://robbiblubber.lima-city.de/utsto/res/info.png";
            WarningIcon = "http://robbiblubber.lima-city.de/utsto/res/warn.png";
            SuccessIcon = "http://robbiblubber.lima-city.de/utsto/res/succ.png";
            FailureIcon = "http://robbiblubber.lima-city.de/utsto/res/fail.png";

            TestSuccessIcon = "http://robbiblubber.lima-city.de/utsto/res/lsucc.png";
            TestFailureIcon = "http://robbiblubber.lima-city.de/utsto/res/lfail.png";

            CssFile = "http://robbiblubber.lima-city.de/utsto/css/default.css";

            _Body = new StringBuilder();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public events                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Risen when object has been updated.</summary>
        public event EventHandler Updated;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the information icon URL.</summary>
        public string NullIcon { get; set; }


        /// <summary>Gets or sets the information icon URL.</summary>
        public string InfoIcon { get; set; }


        /// <summary>Gets or sets the warning icon URL.</summary>
        public string WarningIcon { get; set; }


        /// <summary>Gets or sets the success icon URL.</summary>
        public string SuccessIcon { get; set; }


        /// <summary>Gets or sets the failure icon URL.</summary>
        public string FailureIcon { get; set; }


        /// <summary>Gets or sets the test success icon URL.</summary>
        public string TestSuccessIcon { get; set; }


        /// <summary>Gets or sets the test failure icon URL.</summary>
        public string TestFailureIcon { get; set; }


        /// <summary>Gets or sets the CSS file URL.</summary>
        public string CssFile { get; set; }


        /// <summary>Gets the HTML text for this instance.</summary>
        public string HTML
        {
            get { return (_LEAD.Replace("%CSS%", CssFile) + _Body.ToString() + _TAIL); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Clears the contents of this instance.</summary>
        public void Clear()
        {
            _Body = new StringBuilder();
            Updated?.Invoke(this, new EventArgs());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITestHost                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a message.</summary>
        /// <param name="text">Text.</param>
        /// <param name="type">Message type.</param>
        public void Write(string text, MessageType type = MessageType.NONE)
        {
            _Body.Append("<img src=\"");

            switch(type)
            {
                case MessageType.INFORMATION:
                    _Body.Append(InfoIcon); break;
                case MessageType.WARNING:
                    _Body.Append(WarningIcon); break;
                case MessageType.SUCCESS:
                    _Body.Append(SuccessIcon); break;
                case MessageType.FAILURE:
                    _Body.Append(FailureIcon); break;
                default:
                    _Body.Append(NullIcon); break;
            }
            _Body.Append("\" />&nbsp;&nbsp;&nbsp;");
            _Body.Append(text.Replace("\r", "").Replace("\n", "<br/>\n"));
            _Body.Append("<br/>\n\n");

            Updated?.Invoke(this, new EventArgs());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITestHostWriter                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a test sequence header.</summary>
        /// <param name="caption">Caption.</param>
        /// <param name="text">Text.</param>
        void ITestHostWriter.WriteHeader(string caption, string text)
        {
            _Body.Append("<table width=\"100%\"><tr><td>\n<br/><b>");
            _Body.Append(caption.Replace("\r", "").Replace("\n", "<br/>\n"));
            _Body.Append("</b><br/>\n");
            _Body.Append(text.Replace("\r", "").Replace("\n", "</b><br/>\n"));
            _Body.Append("<br/>\n<br/>\n</td></tr></table>\n<br/><br/>\n\n");

            Updated?.Invoke(this, new EventArgs());
        }


        /// <summary>Writes a unit test header.</summary>
        /// <param name="test">Unit test.</param>
        void ITestHostWriter.WriteTestHeader(IUnitTest test)
        {
            _Body.Append("<br/>\n<hr/>\n<b>");
            _Body.Append(test.Name);
            _Body.Append("</b><br/>\n");
            _Body.Append("<font class=\"text_grey\">");
            _Body.Append(test.Description.Replace("\r", "").Replace("\n", "<br/>\n"));
            _Body.Append("</font><hr/><br/>\n<br/>");

            Updated?.Invoke(this, new EventArgs());
        }


        /// <summary>Writes a test result.</summary>
        /// <param name="test">Unit test.</param>
        /// <param name="result">Result.</param>
        void ITestHostWriter.WriteTestResult(IUnitTest test, bool result)
        {
            _Body.Append("<br/>\n<img src=\"");
            _Body.Append(result ? TestSuccessIcon : TestFailureIcon);
            _Body.Append("\" />&nbsp;&nbsp;&nbsp;<b>");
            _Body.Append(test.Name);
            _Body.Append(result ? "</b> has been completed successfully.<br/>\n<br/>\n" : "</b> has failed.<br/>\n<br/>\n");

            Updated?.Invoke(this, new EventArgs());
        }


        /// <summary>Writes a test sequence result.</summary>
        /// <param name="result">Result.</param>
        /// <param name="text">Text.</param>
        void ITestHostWriter.WriteResult(bool result, string text)
        {
            if(text == null) { text = (result ? "All tests have been completed successfully." : "All tests have been completed. Some test have failed."); }

            _Body.Append("<br/>\n<br/>\n<table width=\"100%\"><tr><td class=\"bg_grey\">\n<br/>\n");
            _Body.Append("<img src=\"");
            _Body.Append(result ? TestSuccessIcon : TestFailureIcon);
            _Body.Append("\" />&nbsp;&nbsp;&nbsp;");
            _Body.Append(text.Replace("\r", "").Replace("\n", "<br/>\n"));
            _Body.Append("<br/>\n<br/>\n</td></tr></table>\n<br/><br/><br/>\n");

            Updated?.Invoke(this, new EventArgs());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the HTML contents of this instance as a string.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return HTML;
        }
    }
}
