﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a JAR library.</summary>
    public class JarLibrary: TLibrary, IUnitTest, IEnumerable<IUnitTest>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static members                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Java initialization string.</summary>
        protected static string _JavaInit = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="file">File name.</param>
        public JarLibrary(string file): base()
        {
            _FileName = Path.GetFullPath(file);

            string temp = FileOp.CreateTempPath();
            string ctrl = (temp + @"\control.ddp");
            string rslt = (temp + @"\result.ddp");

            Ddp ddp = new DdpString();

            ddp.SetValue("op", "load");
            ddp.SetValue("file", _FileName);
            ddp.Save(ctrl);

            __JavaInvoke(temp);

            while(File.Exists(ctrl)) { Thread.Sleep(60); }

            ddp = Ddp.Load(rslt);

            Name = ddp.GetString("name");
            Version = new Version(ddp.GetString("version", "1.0"));

            int count = ddp.GetInteger("count");

            for(int i = 0; i < count; i++)
            {
                _Items.Add(new __JarTestWrapper(ddp, _FileName, i));
            }

            Directory.Delete(temp, true);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static properties                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the java initialization string.</summary>
        protected internal static string __JavaInit
        {
            get
            {
                if(_JavaInit == null)
                {
                    Ddp cfg = Ddp.Load(PathOp.ApplicationDirectory + @"\java.init");

                    _JavaInit = cfg.GetString("/java/startup", "java -jar");
                }

                return _JavaInit;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Invokes the java interop program.</summary>
        /// <param name="temp">Temporary folder.</param>
        protected internal static void __JavaInvoke(string temp)
        {
            Process.Start(__JavaInit + " " + PathOp.ApplicationDirectory + "\\Robbiblubber.Util.Test.Interop.jar \"" + temp + "\"");
        }
    }
}
