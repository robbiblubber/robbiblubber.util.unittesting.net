﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a test host for the console.</summary>
    public class ConsoleTestHost: ITestHost, ITestHostWriter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Break line.</summary>
        private const string _BreakLine = "--------------------------------------------------------------------------------";
        
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ConsoleTestHost()
        {
            ConsoleColor fallback = Console.ForegroundColor;

            switch(Console.BackgroundColor)
            {
                case ConsoleColor.Black:
                case ConsoleColor.DarkBlue:
                case ConsoleColor.DarkGray:
                case ConsoleColor.DarkGreen:
                case ConsoleColor.DarkMagenta:
                case ConsoleColor.DarkRed:
                case ConsoleColor.DarkYellow:
                    DefaultTextColor = ConsoleColor.Gray;
                    InformationTextColor = ConsoleColor.White;
                    SuccessTextColor = ConsoleColor.Green;
                    WarningTextColor = ConsoleColor.Yellow;
                    FailureTextColor = ConsoleColor.Red;
                    break;
                default:
                    DefaultTextColor = ConsoleColor.DarkGray;
                    InformationTextColor = ConsoleColor.Black;
                    SuccessTextColor = ConsoleColor.DarkGreen;
                    WarningTextColor = ConsoleColor.DarkYellow;
                    FailureTextColor = ConsoleColor.DarkRed;
                    break;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the default text color.</summary>
        public ConsoleColor DefaultTextColor
        {
            get; set;
        }


        /// <summary>Gets or sets the information text color.</summary>
        public ConsoleColor InformationTextColor
        {
            get; set;
        }


        /// <summary>Gets or sets the success text color.</summary>
        public ConsoleColor SuccessTextColor
        {
            get; set;
        }


        /// <summary>Gets or sets the warning text color.</summary>
        public ConsoleColor WarningTextColor
        {
            get; set;
        }


        /// <summary>Gets or sets the failure text color.</summary>
        public ConsoleColor FailureTextColor
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITestHost                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a message.</summary>
        /// <param name="text">Text.</param>
        /// <param name="type">Message type.</param>
        public void Write(string text, MessageType type = MessageType.NONE)
        {
            switch(type)
            {
                case MessageType.FAILURE:
                    Console.ForegroundColor = FailureTextColor;
                    Console.Write("   x ");
                    break;
                case MessageType.WARNING:
                    Console.ForegroundColor = WarningTextColor;
                    Console.Write("   ! ");
                    break;
                case MessageType.SUCCESS:
                    Console.ForegroundColor = SuccessTextColor;
                    Console.Write("   * ");
                    break;
                case MessageType.INFORMATION:
                    Console.ForegroundColor = InformationTextColor;
                    Console.Write("   i ");
                    break;
                default:
                    Console.ForegroundColor = DefaultTextColor;
                    Console.Write("     ");
                    break;
            }

            Console.WriteLine(text.Replace("\r\n", "\n").Replace("\n", "\r\n     "));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITestHostWriter                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a test header.</summary>
        /// <param name="caption">Caption.</param>
        /// <param name="text">Text.</param>
        void ITestHostWriter.WriteHeader(string caption, string text)
        {
            Console.ForegroundColor = InformationTextColor;
            Console.WriteLine(caption);
            Console.ForegroundColor = DefaultTextColor;
            Console.WriteLine(text);
            Console.WriteLine();
            Console.WriteLine();
        }


        /// <summary>Writes a unit test header.</summary>
        /// <param name="test">Unit test.</param>
        void ITestHostWriter.WriteTestHeader(IUnitTest test)
        {
            Console.ForegroundColor = DefaultTextColor;
            Console.WriteLine(_BreakLine);

            Console.ForegroundColor = InformationTextColor;
            Console.WriteLine(test.Name);

            Console.ForegroundColor = DefaultTextColor;
            Console.WriteLine();
            Console.WriteLine(test.Description);
            Console.WriteLine(_BreakLine);
            Console.WriteLine();
        }


        /// <summary>Writes a test result.</summary>
        /// <param name="test">Unit test.</param>
        /// <param name="result">Result.</param>
        void ITestHostWriter.WriteTestResult(IUnitTest test, bool result)
        {
            Console.ForegroundColor = InformationTextColor;
            Console.Write(test.Name);

            if(result)
            {
                Console.ForegroundColor = SuccessTextColor;
                Console.WriteLine(" has been completed successfully.");
            }
            else
            {
                Console.ForegroundColor = FailureTextColor;
                Console.WriteLine(" has failed.");
            }
        }


        /// <summary>Writes a test sequence result.</summary>
        /// <param name="result">Result.</param>
        /// <param name="text">Text.</param>
        public void WriteResult(bool result, string text = null)
        {
            Console.WriteLine();
            if(result)
            {
                if(text == null) { text = "All tests have been completed successfully."; }

                Console.ForegroundColor = SuccessTextColor;
                Console.WriteLine(_BreakLine);
                Console.WriteLine(text);
                Console.WriteLine(_BreakLine);
            }
            else
            {
                if(text == null) { text = "All tests have been completed. Some test have failed."; }

                Console.ForegroundColor = FailureTextColor;
                Console.WriteLine(_BreakLine);
                Console.WriteLine(text);
                Console.WriteLine(_BreakLine);
            }
        }
    }
}
