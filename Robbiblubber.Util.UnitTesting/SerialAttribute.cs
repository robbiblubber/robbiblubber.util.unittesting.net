﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This attribute marks a unit test class as requiring serial execution.</summary>
    public class SerialAttribute: Attribute
    {}
}
