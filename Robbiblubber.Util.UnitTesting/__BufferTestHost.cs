﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a buffer test host.</summary>
    internal class __BufferTestHost: ITestHost, ITestHostWriter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Buffered value.</summary>
        private List<_BufferedValue> _Values = new List<_BufferedValue>();


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        internal __BufferTestHost()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes the buffered values to another host.</summary>
        /// <param name="host">Test host.</param>
        public void Write(ITestHost host)
        {
            foreach(_BufferedValue i in _Values)
            {
                i.Write(host);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITestHost                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a message.</summary>
        /// <param name="text">Text.</param>
        /// <param name="type">Message type.</param>
        public void Write(string text, MessageType type = MessageType.NONE)
        {
            _BufferedValue v = new _BufferedValue();
            v.Type = "write";
            v.Text = text;
            v.MessageType = type;

            _Values.Add(v);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ITestHostWriter                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a test header.</summary>
        /// <param name="caption">Caption.</param>
        /// <param name="text">Text.</param>
        void ITestHostWriter.WriteHeader(string caption, string text)
        {
            _BufferedValue v = new _BufferedValue();
            v.Type = "writeheader";
            v.Caption = caption;
            v.Text = text;

            _Values.Add(v);
        }


        /// <summary>Writes a unit test header.</summary>
        /// <param name="test">Unit test.</param>
        void ITestHostWriter.WriteTestHeader(IUnitTest test)
        {
            _BufferedValue v = new _BufferedValue();
            v.Type = "writetestheader";
            v.Test = test;

            _Values.Add(v);
        }


        /// <summary>Writes a test result.</summary>
        /// <param name="test">Unit test.</param>
        /// <param name="result">Result.</param>
        void ITestHostWriter.WriteTestResult(IUnitTest test, bool result)
        {
            _BufferedValue v = new _BufferedValue();
            v.Type = "writetestresult";
            v.Test = test;
            v.Result = result;

            _Values.Add(v);
        }


        /// <summary>Writes a test sequence result.</summary>
        /// <param name="result">Result.</param>
        /// <param name="text">Text.</param>
        public void WriteResult(bool result, string text = null)
        {
            _BufferedValue v = new _BufferedValue();
            v.Type = "writeresult";
            v.Result = result;
            v.Text = text;

            _Values.Add(v);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] _BufferedValue                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class represents a buffered value.</summary>
        private class _BufferedValue
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public members                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Buffered value type.</summary>
            public string Type;

            /// <summary>Message type.</summary>
            public MessageType MessageType;

            /// <summary>Caption.</summary>
            public string Caption;

            /// <summary>Text.</summary>
            public string Text;

            /// <summary>Unit test.</summary>
            public IUnitTest Test;

            /// <summary>Result.</summary>
            public bool Result;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public methods                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Writes the buffered value to another host.</summary>
            /// <param name="host">Test host.</param>
            public void Write(ITestHost host)
            {
                if(Type.ToLower() == "write")
                {
                    host.Write(Text, MessageType);
                    return;
                }

                if(!(host is ITestHostWriter)) return;

                switch(Type.ToLower())
                {
                    case "writetestheader":
                        ((ITestHostWriter) host).WriteTestHeader(Test); break;
                    case "writeheader":
                        ((ITestHostWriter) host).WriteHeader(Caption, Text); break;
                    case "writetestresult":
                        ((ITestHostWriter) host).WriteTestResult(Test, Result); break;
                    case "writeresult":
                        ((ITestHostWriter) host).WriteResult(Result, Text); break;
                }
            }
        }
    }
}
