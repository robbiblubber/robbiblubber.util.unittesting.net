﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This attribute marks a method as prepare method.</summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PrepareAttribute: Attribute
    {}
}
