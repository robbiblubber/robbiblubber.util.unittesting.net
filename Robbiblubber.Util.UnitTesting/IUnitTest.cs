﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>Unit tests implement this interface.</summary>
    public interface IUnitTest: IComparable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Prepares the test environment.</summary>
        /// <param name="host">Host.</param>
        void Prepare(ITestHost host);


        /// <summary>Cleans up the test environment.</summary>
        /// <param name="host">Host.</param>
        void Cleanup(ITestHost host);


        /// <summary>Runs a test.</summary>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was completed successful, otherwise returns FALSE.</returns>
        bool Run(ITestHost host);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the test name.</summary>
        string Name { get; }


        /// <summary>Gets the test description.</summary>
        string Description { get; }


        /// <summary>Gets the file name.</summary>
        string FileName { get; }


        /// <summary>Gets the class name.</summary>
        string ClassName { get; }


        /// <summary>Gets the sort index.</summary>
        int SortIndex { get; set; }


        /// <summary>Gets if the unit test can be run in parallel independent from other unit tests.</summary>
        bool Parallelizable { get; }


        /// <summary>Gets or sets the unit test data.</summary>
        object Data { get; set; }
    }
}
