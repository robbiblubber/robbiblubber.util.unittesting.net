﻿using System;
using System.Reflection;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This attribute will define the execution order of methods marked with the same attribute (Test, Prepare, Cleanup).</summary>
    public class MethodOrderAttribute: Attribute, IComparable
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal protected members                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Method.</summary>
        internal protected MethodInfo __Method;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        internal protected MethodOrderAttribute()
        {
            SortIndex = 1024;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="sortIndex">Sort index.</param>
        public MethodOrderAttribute(int sortIndex)
        {
            SortIndex = sortIndex;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the sort index of this instance.</summary>
        public int SortIndex
        {
            get; protected set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares this instance to an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Comparison result.</returns>
        int IComparable.CompareTo(object obj)
        {
            if(obj is MethodOrderAttribute)
            {
                return SortIndex.CompareTo(((MethodOrderAttribute) obj).SortIndex);
            }

            return -1;
        }
    }
}
