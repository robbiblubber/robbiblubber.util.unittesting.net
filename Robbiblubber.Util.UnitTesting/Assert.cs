﻿using System;

using Robbiblubber.Util.UnitTesting.Exceptions;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class provides assertion methods.</summary>
    public static partial class Assert
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Asserts success is true.</summary>
        /// <param name="success">Success value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void Probe(bool success, string message = null)
        {
            if(!success)
            {
                throw new AssertionException((message == null) ? "Assertion failed." : "Assertion failed. " + message);
            }
        }


        /// <summary>Indicates that the assertion has failed and throws an exception.</summary>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void Fail(string message = null)
        {
            throw new AssertionException((message == null) ? "Assertion failed." : "Assertion failed. " + message);
        }


        /// <summary>Asserts a value is TRUE.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void IsTrue(bool obtained, string message = null)
        {
            if(!obtained)
            {
                if(message == null) { message = "Expected TRUE."; }
                throw new AssertionException("Assertion failed. " + message);
            }
        }


        /// <summary>Asserts a value is FALSE.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void IsFalse(bool obtained, string message = null)
        {
            if(obtained)
            {
                if(message == null) { message = "Expected FALSE."; }
                throw new AssertionException("Assertion failed. " + message);
            }
        }


        /// <summary>Asserts a value is NULL.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void IsNull(object obtained, string message = null)
        {
            if(obtained != null)
            {
                if(message == null) { message = "Expected NULL."; }
                throw new AssertionException("Assertion failed. " + message);
            }
        }


        /// <summary>Asserts a value is not NULL.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void NotNull(object obtained, string message = null)
        {
            if(obtained == null)
            {
                if(message == null) { message = "Expected NOT NULL."; }
                throw new AssertionException("Assertion failed. " + message);
            }
        }


        /// <summary>Asserts expected and obtained strings are equal.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="message">Message.</param>
        /// <param name="ignoreCase">Ignore case flag.</param>
        /// <exception cref="EqualityAssertionException">Thrown when the assertion fails.</exception>
        public static void Equal(string obtained, string expected, string message = null, bool ignoreCase = false)
        {
            if(expected == null)
            {
                if(obtained != null) { throw new EqualityAssertionException(EqualityAssertionException.__GetMessage(expected, obtained, message)); }
                return;
            }
            if(obtained == null) { throw new EqualityAssertionException(EqualityAssertionException.__GetMessage(expected, obtained, message)); }

            if(ignoreCase)
            {
                if(!expected.ToLower().Equals(obtained.ToLower()))
                {
                    throw new EqualityAssertionException(EqualityAssertionException.__GetMessage(expected, obtained, message));
                }
            }
            else if(!expected.Equals(obtained))
            {
                throw new EqualityAssertionException(EqualityAssertionException.__GetMessage(expected, obtained, message));
            }
        }


        /// <summary>Asserts expected and obtained strings are equal.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="ignoreCase">Ignore case flag.</param>
        /// <exception cref="EqualityAssertionException">Thrown when the assertion fails.</exception>
        public static void Equal(string obtained, string expected, bool ignoreCase)
        {
            Equal(obtained, expected, null, ignoreCase);
        }


        /// <summary>Asserts expected and obtained values are equal.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="EqualityAssertionException">Thrown when the assertion fails.</exception>
        public static void Equal(object obtained, object expected, string message = null)
        {
            if(expected == null)
            {
                if(obtained != null) { throw new EqualityAssertionException(EqualityAssertionException.__GetMessage(expected, obtained, message)); }
                return;
            }

            if(!expected.Equals(obtained))
            {
                throw new EqualityAssertionException(EqualityAssertionException.__GetMessage(expected, obtained, message));
            }
        }


        /// <summary>Asserts expected and obtained strings are not equal.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="message">Message.</param>
        /// <param name="ignoreCase">Ignore case flag.</param>
        /// <exception cref="EqualityAssertionException">Thrown when the assertion fails.</exception>
        public static void NotEqual(string obtained, string expected, string message = null, bool ignoreCase = false)
        {
            if(expected == null)
            {
                if(obtained == null) throw new EqualityAssertionException("Assertion failed. Obtained value is expected not to be " + EqualityAssertionException.__ToString(obtained) + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
                return;
            }
            if(obtained == null) return;

            if(ignoreCase)
            {
                if(!expected.ToLower().Equals(obtained.ToLower()))
                {
                    throw new EqualityAssertionException("Assertion failed. Obtained value is expected not to be " + EqualityAssertionException.__ToString(obtained) + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
                }
                return;
            }

            if(!expected.Equals(obtained))
            {
                throw new EqualityAssertionException("Assertion failed. Obtained value is " + EqualityAssertionException.__ToString(obtained));
            }
        }


        /// <summary>Asserts expected and obtained strings are not equal.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="ignoreCase">Ignore case flag.</param>
        /// <exception cref="EqualityAssertionException">Thrown when the assertion fails.</exception>
        public static void NotEqual(string obtained, string expected, bool ignoreCase)
        {
            NotEqual(obtained, expected, null, false);
        }


        /// <summary>Asserts expected and obtained strings are equal.</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="EqualityAssertionException">Thrown when the assertion fails.</exception>
        public static void NotEqual(object obtained, object expected, string message = null)
        {
            if(expected == null)
            {
                if(obtained == null) throw new EqualityAssertionException("Assertion failed. Obtained value is expected not to be " + EqualityAssertionException.__ToString(obtained) + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
                return;
            }

            if(expected.Equals(obtained))
            {
                throw new EqualityAssertionException("Assertion failed. Obtained value is expected not to be " + EqualityAssertionException.__ToString(obtained) + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
            }
        }


        /// <summary>Asserts expected and obtained strings are identical (references of the same instance).</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void Identical(object obtained, object expected, string message = null)
        {
            if(!ReferenceEquals(obtained, expected))
            {
                throw new IdentityAssertionException("Assertion failed. Identity expected: " + EqualityAssertionException.__ToString(obtained) + "." + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
            }
        }


        /// <summary>Asserts expected and obtained strings are not identical (references of the same instance).</summary>
        /// <param name="obtained">Obtained value.</param>
        /// <param name="expected">Expected value.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void NotIdentical(object obtained, object expected, string message = null)
        {
            if(ReferenceEquals(obtained, expected))
            {
                throw new IdentityAssertionException("Assertion failed. Identity unexpected: " + EqualityAssertionException.__ToString(obtained) + "." + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
            }
        }


        /// <summary>Asserts the code throws an exception.</summary>
        /// <param name="exception">Exception type.</param>
        /// <param name="code">Executable method.</param>
        /// <param name="message">Message.</param>
        /// <exception cref="AssertionException">Thrown when the assertion fails.</exception>
        public static void ExceptionThrown(Type exception, Action code, string message = null)
        {
            try
            {
                code();
            }
            catch(Exception ex)
            {
                if(!exception.IsAssignableFrom(ex.GetType()))
                {
                    throw new AssertionException("Assertion failed. Expected exception: " + exception.Name + "; thrown: " + ex.GetType().Name + "." + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
                }

                return;
            }

            throw new AssertionException("Assertion failed. Expected exception: " + exception.Name + "; no exception thrown." + (string.IsNullOrWhiteSpace(message) ? "" : (" " + message)));
        }
    }
}
