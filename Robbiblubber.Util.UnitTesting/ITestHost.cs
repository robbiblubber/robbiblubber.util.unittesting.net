﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>A test host implements this interface.</summary>
    public interface ITestHost
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a message.</summary>
        /// <param name="text">Text.</param>
        /// <param name="type">Message type.</param>
        void Write(string text, MessageType type = MessageType.NONE);
    }



    /// <summary>This enumeration defines message types.</summary>
    public enum MessageType
    {
        /// <summary>No message type.</summary>
        NONE = 0,
        /// <summary>Information message.</summary>
        INFORMATION = 1,
        /// <summary>Warning message.</summary>
        WARNING = 2,
        /// <summary>Failure message.</summary>
        FAILURE = 3,
        /// <summary>Success message.</summary>
        SUCCESS = 4
    }
}
