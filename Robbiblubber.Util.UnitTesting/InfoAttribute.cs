﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This attribute allows pinning information on a class or method.</summary>
    public class InfoAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="text">Text</param>
        public InfoAttribute(string text)
        {
            Text = text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public proprties                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the information text.</summary>
        public string Text
        {
            get; protected set;
        }
    }
}
