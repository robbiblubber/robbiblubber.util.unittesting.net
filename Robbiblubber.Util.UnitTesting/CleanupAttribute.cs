﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This attribute marks a method as cleanup method.</summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CleanupAttribute: Attribute
    {}
}
