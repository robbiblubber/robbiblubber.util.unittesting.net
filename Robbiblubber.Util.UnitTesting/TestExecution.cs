﻿using System;
using System.Threading;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class provides the environment for asynchronous test execution.</summary>
    public class TestExecution
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Execution thread.</summary>
        private Thread _T = null;


        /// <summary>Test.</summary>
        private IUnitTest _Test = null;


        /// <summary>Host.</summary>
        private __BufferTestHost _Host = null;


        /// <summary>Success flag.</summary>
        private bool _Success = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="test">Unit test.</param>
        public TestExecution(IUnitTest test)
        {
            _Host = new __BufferTestHost();
            _Test = test;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the current test execution state.</summary>
        public ExecutionState State
        {
            get
            {
                if(_T == null) return ExecutionState.READY;
                if(_T.IsAlive) return ExecutionState.EXECUTING;

                return (_Success ? ExecutionState.SUCCEEDED : ExecutionState.FAILED);
            }
        }


        /// <summary>Gets if the test is closed.</summary>
        public bool Closed
        {
            get { return _Host == null; }
        }


        /// <summary>Gets the test.</summary>
        public IUnitTest Test
        {
            get { return _Test; }
        }


        /// <summary>Gets the host.</summary>
        public ITestHost Host
        {
            get
            {
                if(_Host == null) { _Host = new __BufferTestHost(); }
                return _Host;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Executes the test.</summary>
        public void Execute()
        {
            if(_Host == null) { _Host = new __BufferTestHost(); }
            _T = new Thread(new ThreadStart(_Execute));
            _T.Start();
        }


        /// <summary>Writes the test output to a host and closes the test.</summary>
        /// <param name="host">Test host.</param>
        public void WriteAndClose(ITestHost host)
        {
            if(Closed) return;

            _Host.Write(host);
            _Host = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Performs the Prepare(), Run(), an Cleanup() methods on the unit test.</summary>
        private void _Execute()
        {
            _Test.Prepare(_Host);
            _Success = _Test.Run(_Host);
            _Test.Cleanup(_Host);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] ResultStates                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enumeration defines possible test execution states.</summary>
        public enum ExecutionState: int
        {
            /// <summary>Test is ready.</summary>
            READY = 0,
            /// <summary>Test is executing.</summary>
            EXECUTING = 1,
            /// <summary>Test has succeeded.</summary>
            SUCCEEDED = 2,
            /// <summary>Test has failed.</summary>
            FAILED = 4
        }
    }
}
