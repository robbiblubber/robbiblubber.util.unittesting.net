﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>Classes marked with this attribute will be searched for test methods also marked with this attribute.</summary>
    public class TestAttribute: Attribute
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Test name.</param>
        /// <param name="description">Test description.</param>
        public TestAttribute(string name = null, string description = null)
        {
            Name = name;
            Description = description;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the test name.</summary>
        public string Name
        {
            get; protected set;
        }


        /// <summary>Gets the test description.</summary>
        public string Description
        {
            get; protected set;
        }
    }
}
