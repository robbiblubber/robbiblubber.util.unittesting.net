﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements extension methods for test host communication.</summary>
    public static class TestHostWriterExtensionscs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes the exception message to test host.</summary>
        /// <param name="e">Exception.</param>
        /// <param name="host">Host.</param>
        /// <param name="type">Message type.</param>
        public static void WriteToHost(this Exception e, ITestHost host, MessageType type = MessageType.FAILURE)
        {
            host.Write(e.Message, type);
        }
    }
}
