﻿using System;



namespace Robbiblubber.Util.UnitTesting.Exceptions
{
    /// <summary>This class implements an assertion exception.</summary>
    public class AssertionException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner Exception.</param>
        public AssertionException(string message, Exception innerException = null): base(message, innerException)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="innerException">Inner Exception.</param>
        public AssertionException(Exception innerException = null) : base("Assertion failed.", innerException)
        {}
    }
}
