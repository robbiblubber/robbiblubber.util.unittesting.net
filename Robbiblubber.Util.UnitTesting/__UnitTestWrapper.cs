﻿using System;
using System.IO;
using System.Reflection;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This class implements a IUnitTest wrapper for attributed test classes.</summary>
    internal sealed class __UnitTestWrapper: IUnitTest
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Test class type.</summary>
        private Type _Type = null;


        /// <summary>Test class instance.</summary>
        private object _Instance = null;


        /// <summary>Sort index.</summary>
        private int _SortIndex = 1024;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="t">Test type.</param>
        public __UnitTestWrapper(Type t)
        {
            _Type = t;
            try { _Instance = Activator.CreateInstance(t); } catch(Exception) {}
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IUnitTest                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Prepares the test environment.</summary>
        /// <param name="host">Host.</param>
        public void Prepare(ITestHost host)
        {
            foreach(MethodInfo i in UnitTest.GetMethods(_Type, typeof(PrepareAttribute)))
            {
                try
                {
                    if(i.GetParameters().Length == 1)
                    {
                        i.Invoke(_Instance, new object[] { host });
                    }
                    else
                    {
                        i.Invoke(_Instance, new object[0]);
                    }
                }
                catch(Exception ex) { host.Write("Prepare (" + i.Name + ") failed. " + ex.Message, MessageType.WARNING); }
            }
        }


        /// <summary>Cleans up the test environment.</summary>
        /// <param name="host">Host.</param>
        public void Cleanup(ITestHost host)
        {
            foreach(MethodInfo i in UnitTest.GetMethods(_Type, typeof(CleanupAttribute)))
            {
                try
                {
                    if(i.GetParameters().Length == 1)
                    {
                        i.Invoke(_Instance, new object[] { host });
                    }
                    else
                    {
                        i.Invoke(_Instance, new object[0]);
                    }
                }
                catch(Exception ex) { host.Write("Cleanup (" + i.Name + ") failed. " + ex.Message, MessageType.WARNING); }
            }
        }


        /// <summary>Runs a test.</summary>
        /// <param name="host">Host.</param>
        /// <returns>Returns TRUE if the test was completed successful, otherwise returns FALSE.</returns>
        public bool Run(ITestHost host)
        {
            return UnitTest.RunInstance(_Type, _Instance, host);
        }


        /// <summary>Gets the test name.</summary>
        public string Name
        {
            get
            {
                string rval = ((TestAttribute) UnitTest.GetAttribute(_Type, typeof(TestAttribute))).Name;
                if(string.IsNullOrWhiteSpace(rval)) { rval = _Type.Name; }

                return rval;
            }
        }


        /// <summary>Gets the test description.</summary>
        public string Description
        {
            get
            {
                return ((TestAttribute) UnitTest.GetAttribute(_Type, typeof(TestAttribute))).Description;
            }
        }


        /// <summary>Gets the library file name.</summary>
        public string FileName
        {
            get
            {
                UriBuilder b = new UriBuilder(_Type.Assembly.CodeBase);
                return Path.GetFullPath(Uri.UnescapeDataString(b.Path));
            }
        }


        /// <summary>Gets the class name.</summary>
        public string ClassName
        {
            get { return _Type.FullName; }
        }



        /// <summary>Gets if the unit test can be run in parallel independent from other unit tests.</summary>
        public bool Parallelizable
        {
            get
            {
                return (UnitTest.GetAttribute(_Type, typeof(SerialAttribute)) != null);
            }
        }


        /// <summary>Gets or sets the sort index.</summary>
        public int SortIndex
        {
            get { return _SortIndex; }
            set { _SortIndex = value; }
        }


        /// <summary>Gets or sets the unit test data.</summary>
        public object Data
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares this instance to an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Comparison result.</returns>
        int IComparable.CompareTo(object obj)
        {
            if(obj is IUnitTest)
            {
                if(((IUnitTest) obj).SortIndex == _SortIndex)
                {
                    return Name.CompareTo(((IUnitTest) obj).Name);
                }
                return _SortIndex.CompareTo(((IUnitTest) obj).SortIndex);
            }

            return -1;
        }
    }
}
