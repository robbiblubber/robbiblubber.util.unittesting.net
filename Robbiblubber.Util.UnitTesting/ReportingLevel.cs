﻿using System;



namespace Robbiblubber.Util.UnitTesting
{
    /// <summary>This enumeration defines reporting levels.</summary>
    public enum ReportingLevel: int
    {
        /// <summary>Minimum reporting level.</summary>
        MINIMUM = 0,
        /// <summary>Standard reporting level.</summary>
        STANDARD = 200,
        /// <summary>Detailed reporting level.</summary>
        DETAILED = 500
    }
}
